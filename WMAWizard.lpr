program WMAWizard;

{
  Working Memory Analyser (Wizard Version 1.0.3)

  An open source Psychology software to investigate both individual
  differences in working memory capacity and the effect of working
  memory load on visual distractibility

  Copyright © 2020-2021 by Phil Duke and Giorgio Fuggetta

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
}

{
Special Thanks to:

 - skalogryz (email: unspecified), additional developed, optimised and compiled the software for MacOS.
 - Howard Page-Clark (email: pageclarkhoward@gmail.com), refactored earlier code to make the experiment
   resource-based rather than file-based, and developed the wizard interface and data-processing modules.
}

{$mode objfpc}{$H+}

uses
  Interfaces, Forms,
  uMain, uParams;

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Scaled := True;
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.

