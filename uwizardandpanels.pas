unit uWizardAndPanels;

{
  Working Memory Analyser (Wizard Version 1.0.3)

  An open source Psychology software to investigate both individual
  differences in working memory capacity and the effect of working
  memory load on visual distractibility

  Copyright © 2020-2021 by Phil Duke and Giorgio Fuggetta

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
}

{
Special Thanks to:

 - skalogryz (email: unspecified), additional developed, optimised and compiled the software for MacOS.
 - Howard Page-Clark (email: pageclarkhoward@gmail.com), refactored earlier code to make the experiment
   resource-based rather than file-based, and developed the wizard interface and data-processing modules.
}

{$mode ObjFPC}{$H+}
{$IfDef lclcocoa}
{$Define applemainmenu}
{$EndIf}
{$WARN 5024 off : Parameter "$1" not used}

interface

uses
  Classes, SysUtils,
  StdCtrls, ExtCtrls, Graphics, Spin, Controls, ComCtrls,
  IPHtml,
  uWordMap;

const

  Margin = 15;
  DblMargin = Margin shl 1;

type

  EPanelKind = (pkIntro, pkConsent, pkUniqueID, pkGetInfo, pkInstructions, pkExperiment, pkFeedback);

  TBooleanEvent = procedure(aBool: Boolean) of object;

  TWizSheet = class;
  TBasePanel = class;

  TPageWizard = class(TCustomControl)
  private
    FOnQuit: TBooleanEvent;
    AboutButton: TButton;
    ButtonPanel: TPanel;
    PageControl: TPageControl;
    QuitButton: TButton;
    WordMap: TWordMap;
    function CreatedOutputFileHeaderOK: Boolean;
    function CurrentPanelKindExists(out PanelKind: EPanelKind): Boolean;
    function GetPanel(aPanelKind: EPanelKind): TBasePanel;
    procedure AboutButtonClick(Sender: TObject);
    procedure AdvancePage;
    procedure DoOnQuit(askForConfirmation: Boolean);
    procedure DoPageCompleted;
    procedure Finish(Sender: TObject);
    procedure NextButtonClick(Sender: TObject);
    procedure QuitButtonClick(Sender: TObject);
    {$IfDef applemainmenu}
    procedure InitAppleMenu;
    {$EndIf}
  protected
    procedure DoMoveNext(CanMove: Boolean);
    procedure SetParent(NewParent: TWinControl); override;
  public
    NextButton: TButton;
    constructor Create(TheOwner: TComponent); override;
    function AddNewSheet(const aTitle, aBrief: String; aPanelKind: EPanelKind): TWizSheet;
    procedure SetAccuracy(anAccuracy: Single);
    procedure ShowFinalPage;
    property OnQuit: TBooleanEvent read FOnQuit write FOnQuit;
  end;

  TWizSheet = class(TTabSheet)
  private
    FPanelKind: EPanelKind;
    FWizard: TPageWizard;
    Panel: TBasePanel;
    TitlePanel: TPanel;
  public
    constructor Create(TheOwner: TPageWizard; const {%H-}aTitle: String; aTitleStyle: TFontStyles; aPanelKind: EPanelKind); reintroduce;
    property PanelKind: EPanelKind read FPanelKind;
    property Wizard: TPageWizard read FWizard;
  end;

  TBasePanel = class(TPanel)
  private
    FWizard: TPageWizard;
    function CanMoveNext: Boolean; virtual;
    procedure DoNotifyWizardOfMoveNext;
  public
    constructor Create(TheOwner: TWizSheet); reintroduce; virtual;
  end;

  TExperimentPanel = class(TBasePanel)
  private
    function CanMoveNext: Boolean; override;
  public
    constructor Create(TheOwner: TWizSheet); override;
  end;

  TIntroPanel = class(TBasePanel)
  private
    Memo: TMemo;
    function CanMoveNext: Boolean; override;
  public
    constructor Create(TheOwner: TWizSheet); override;
  end;

  TConsentPanel = class(TBasePanel)
  private
    EscCheckBox: TCheckBox;
    EscLabel: TLabel;
    ReadCheckBox: TCheckBox;
    ReadLabel: TLabel;
    HTMLPanel: TIpHtmlPanel;
    WithdrawCheckBox: TCheckBox;
    WithdrawLabel: TLabel;
    function CanMoveNext: Boolean; override;
    procedure HTMLPanelHotClick(Sender: TObject);
    procedure CheckBoxChange(Sender: TObject);
    procedure EscLabelClick(Sender: TObject);
    procedure ReadLabelClick(Sender: TObject);
    procedure WithdrawLabelClick(Sender: TObject);
  public
    constructor Create(TheOwner: TWizSheet); override;
  end;

  TUniqueIDPanel = class(TBasePanel)
  private
    fi, mi, yi, da, mo: Integer;
    fiB, miB, yiB, daB, moB, dayMonthB: Boolean;
    DayRG: TRadioGroup;
    FathersInitialRG: TRadioGroup;
    IDLabel: TLabel;
    InstructionLabel: TLabel;
    MonthRG: TRadioGroup;
    MothersInitialRG: TRadioGroup;
    YourInitialRG: TRadioGroup;
    function CanMoveNext: Boolean; override;
    procedure DayMonthRGClick(Sender: TObject);
    function GetCaption: String;
    procedure OnRGChange(Sender: TObject);
  public
    constructor Create(TheOwner: TWizSheet); override;
  end;

  TGetInfoPanel = class(TBasePanel)
  private
    AgeEdit: TSpinEdit;
    AgeGB: TGroupBox;
    ChooseExperimentCombo: TComboBox;
    ChooseExperimentGB: TGroupBox;
    DescriptionMemo: TMemo;
    DisplayLabel: Tlabel;
    ExperimentGB: TGroupBox;
    HandednessRG: TRadioGroup;
    InputFileGB: TGroupBox;
    ParticipantGB: TGroupBox;
    RandomRB: TRadioButton;
    SessionEdit: TSpinEdit;
    SessionGB: TGroupBox;
    SexRG: TRadioGroup;
    SpecificEdit: TSpinEdit;
    SpecificRB: TRadioButton;
    ProgressCB: TCheckBox;

    function CanMoveNext: Boolean; override;
    procedure ExperimentChange(Sender: TObject);
    procedure HandednessSelectionChanged(Sender: TObject);
    procedure RandomSpecificChange(Sender: TObject);
    procedure SexSelectionChanged(Sender: TObject);
  public
    constructor Create(TheOwner: TWizSheet); override;
  end;

  TInstructionPanel = class(TBasePanel)
  private
    Image: TImage;
    function CanMoveNext: Boolean; override;
  public
    constructor Create(TheOwner: TWizSheet); override;
  end;

  TFeedbackPanel = class(TBasePanel)
  private
    FQuURL: String;
    OpenFileButton: TButton;
    QuestionnaireLabel: TLabel;
    ThanksLabel: TLabel;
    TitleLabel: TLabel;
    UploadLabel: TLabel;
    function CanMoveNext: Boolean; override;
    procedure ButtonClick(Sender: TObject);
    procedure QuestionnaireLabelClick(Sender: TObject);
    procedure UploadLabelClick(Sender: TObject);
  public
    constructor Create(TheOwner: TWizSheet); override;
    procedure AdaptForExperimentResult(aResult: Integer);
  end;

implementation

uses
  LCLIntf, Dialogs, Menus, Forms,
  uRStrings, uParams, uTrials, uAboutForm, uTrainingFeedback;

{%region TExperimentPanel}
function TExperimentPanel.CanMoveNext: Boolean;
begin
  Result := True;
end;

constructor TExperimentPanel.Create(TheOwner: TWizSheet);
begin
  inherited Create(TheOwner);
end;
{%endregion TExperimentPanel}

{%region TPageWizard}
constructor TPageWizard.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);

  AboutButton := TButton.Create(Self);
  ButtonPanel := TPanel.Create(Self);
  NextButton := TButton.Create(Self);
  PageControl := TPageControl.Create(Self);
  QuitButton := TButton.Create(Self);
  WordMap := TWordMap.Create(Self);

  WordMap.Align := alTop;
  WordMap.Parent := Self;

  ButtonPanel.Align := alBottom;
  ButtonPanel.ParentColor := True;
  ButtonPanel.BevelOuter := bvNone;
  ButtonPanel.Height := 40;
  ButtonPanel.Parent := Self;

  NextButton.Caption := rsNext;
  NextButton.AutoSize := True;
  NextButton.Anchors := [akTop, akRight];
  NextButton.AnchorSideTop.Control := ButtonPanel;
  NextButton.AnchorSideTop.Side := asrCenter;
  NextButton.AnchorSideRight.Control := ButtonPanel;
  NextButton.AnchorSideRight.Side := asrRight;
  NextButton.BorderSpacing.Right := DblMargin;
  NextButton.Enabled := False;
  NextButton.OnClick := @NextButtonClick;
  NextButton.Parent := ButtonPanel;

  QuitButton.Caption := rsQuit;
  QuitButton.AnchorSideTop.Control := ButtonPanel;
  QuitButton.AnchorSideTop.Side := asrCenter;
  QuitButton.AnchorSideLeft.Control := ButtonPanel;
  QuitButton.BorderSpacing.Left := DblMargin;
  QuitButton.OnClick := @QuitButtonClick;
  QuitButton.Parent := ButtonPanel;

  {$IfDef applemainmenu}
  InitAppleMenu;
  AboutButton.Visible := False;
  {$Else}
  AboutButton.Caption := rsAbout;
  AboutButton.AnchorSideTop.Control := ButtonPanel;
  AboutButton.AnchorSideTop.Side := asrCenter;
  AboutButton.AnchorSideLeft.Control := QuitButton;
  AboutButton.BorderSpacing.Left := Margin;
  AboutButton.AnchorSideLeft.Side := asrRight;
  AboutButton.OnClick := @AboutButtonClick;
  AboutButton.Parent := ButtonPanel;
  {$EndIf}

  PageControl.Align := alClient;
  PageControl.ShowTabs := False;
  PageControl.Color := Color;
  PageControl.Parent := Self;
end;

function TPageWizard.CreatedOutputFileHeaderOK: Boolean;
var
  tf: TextFile;
  c, e: Integer;
begin
  e := 0;
  {$I-}
  AssignFile(tf, ud.OutputFilename); if IOResult <> 0 then Inc(e);
  if FileExists(ud.OutputFilename) then
    Append(tf)
  else Rewrite(tf); if IOResult <> 0 then Inc(e);
  WriteLn(tf, ud.DateStr); if IOResult <> 0 then Inc(e);
  WriteLn(tf, ud.TimeStr); if IOResult <> 0 then Inc(e);
  WriteLn(tf, rsParticipantColon, ud.participantID); if IOResult <> 0 then Inc(e);
  WriteLn(tf, rsAgeColon, ud.age); if IOResult <> 0 then Inc(e);
  WriteLn(tf, rsSexColon, ud.sex, LineEnding); if IOResult <> 0 then Inc(e);
  WriteLn(tf, rsExperimentTab, ud.ExperimentName); if IOResult <> 0 then Inc(e);
  WriteLn(tf, rsPauseTrainingTab, IntToStr(NTrialsBeforePause)); if IOResult <> 0 then Inc(e);
  WriteLn(tf, rsPauseMainTab, IntToStr(NTrialsBeforePause)); if IOResult <> 0 then Inc(e);
  WriteLn(tf, rsInstructionsOdd); if IOResult <> 0 then Inc(e);
  WriteLn(tf, rsInstructionsEven); if IOResult <> 0 then Inc(e);
  WriteLn(tf, rsPauseColor, PauseBackgroundCircleColour.ToCSVStr); if IOResult <> 0 then Inc(e);
  WriteLn(tf, rsRunColor, RunBackgroundCircleColour.ToCSVStr); if IOResult <> 0 then Inc(e);
  WriteLn(tf, rsFixationColor, FixationColour.ToCSVStr); if IOResult <> 0 then Inc(e);
  WriteLn(tf, rsPlaceholdersColor, ud.HoldersColor.ToCSVStr); if IOResult <> 0 then Inc(e);
  WriteLn(tf, rsIncorrectColor, IncorrectFeedbackColour.ToCSVStr); if IOResult <> 0 then Inc(e);
  WriteLn(tf, rsCorrectColor, CorrectFeedbackColour.ToCSVStr); if IOResult <> 0 then Inc(e);
  for c := Low(Colours) to High(Colours) do
    begin WriteLn(tf,Format(rsShapesColor,[c]), Colours[c].ToCSVStr); if IOResult <> 0 then Inc(e); end;
  WriteLn(tf); if IOResult <> 0 then Inc(e);
  WriteLn(tf, rsHeaderLine); if IOResult <> 0 then Inc(e);
  CloseFile(tf); if IOResult <> 0 then Inc(e);
  {$I+}
  Result := e = 0;
end;

function TPageWizard.CurrentPanelKindExists(out PanelKind: EPanelKind): Boolean;
var
  ap: TTabSheet;
begin
  ap := PageControl.ActivePage;
  Result := Assigned(ap);
  if Result then
    PanelKind := TWizSheet(ap).PanelKind;
end;

procedure TPageWizard.Finish(Sender: TObject);
begin
  (Parent as TForm).Close;
end;

function TPageWizard.GetPanel(aPanelKind: EPanelKind): TBasePanel;
var
  i: Integer;
begin
  for i := 0 to PageControl.PageCount-1 do
    if TWizSheet(PageControl.Pages[i]).PanelKind = aPanelKind then
      Exit(TWizSheet(PageControl.Pages[i]).Panel);
  Result := Nil;
end;

procedure TPageWizard.AboutButtonClick(Sender: TObject);
begin
  ShowAboutForm;
end;

procedure TPageWizard.AdvancePage;
begin
  WordMap.ItemIndex := Succ(WordMap.ItemIndex);
  PageControl.PageIndex := Succ(PageControl.ActivePageIndex);
end;

function TPageWizard.AddNewSheet(const aTitle, aBrief: String; aPanelKind: EPanelKind): TWizSheet;
begin
  Assert((aTitle <> '') and (aBrief <> ''), 'Title and brief must be non-blank');
  Result := TWizSheet.Create(Self, aTitle, [fsBold], aPanelKind);
  Result.PageControl := PageControl;
  WordMap.AddWord(aBrief);
  WordMap.ItemIndex := Result.PageControl.PageIndex;
end;

procedure TPageWizard.SetAccuracy(anAccuracy: Single);
begin
  Assert(Assigned(PageControl.ActivePage) and (TWizSheet(PageControl.ActivePage).PanelKind = pkExperiment),'TPageWizard.SetAccuracy called incorrectly');
  ShowTrainingFeedbackModal(anAccuracy);
  if anAccuracy > MinimumTrainingAccuracy then
    begin
      NextButton.Caption := rsFinish;
      NextButton.OnClick := @Finish;
      QuitButton.Hide;
      AboutButton.BorderSpacing.Left := DblMargin;
      AdvancePage;
    end;
  ef.MaximiseWindow;
end;

procedure TPageWizard.ShowFinalPage;
begin
  TFeedbackPanel(GetPanel(pkFeedback)).AdaptForExperimentResult(ExperimentResult);
  if TWizSheet(PageControl.ActivePage).PanelKind <> pkFeedback then
    repeat
      AdvancePage;
    until TWizSheet(PageControl.ActivePage).PanelKind = pkFeedback;
  Parent.Show;
end;

procedure TPageWizard.DoMoveNext(CanMove: Boolean);
begin
  if CanMove then
    DoPageCompleted;
  NextButton.Enabled := CanMove;
end;

procedure TPageWizard.DoOnQuit(askForConfirmation: Boolean);
begin
  if Assigned(OnQuit) then
    OnQuit(askForConfirmation);
end;

procedure TPageWizard.DoPageCompleted;
var
  gip: TGetInfoPanel;
  ip: TInstructionPanel;
  bmp: TBitmap;
begin
  if not Assigned(PageControl.ActivePage) then Exit;
  case TWizSheet(PageControl.ActivePage).PanelKind of
    pkUniqueID: begin
                  ud.participantID := TUniqueIDPanel(GetPanel(pkUniqueID)).IDLabel.Caption;
                  gip := TGetInfoPanel(GetPanel(pkGetInfo));
                  if Assigned(gip) then
                    gip.ParticipantGB.Caption := Format(rsDetailsForParticipant, [ud.participantID]);
                end;
    pkGetInfo: begin
                 gip := TGetInfoPanel(GetPanel(pkGetInfo));
                 ud.Init(gip.AgeEdit.Value.ToString, gip.HandednessRG.Items[gip.HandednessRG.ItemIndex], gip.SexRG.Items[gip.SexRG.ItemIndex],
                   Succ(gip.ChooseExperimentCombo.ItemIndex), gip.SpecificEdit.Value, gip.SessionEdit.Value,
                   gip.RandomRB.Checked, gip.ProgressCB.Checked);
                 if not CreatedOutputFileHeaderOK then
                   begin
                     MessageDlg(rsFileWriteError, rsUnableToCreate, mtWarning, [mbOK], 0);
                     DoOnQuit(False);
                   end;

                 ip := TInstructionPanel(GetPanel(pkInstructions));
                 if Assigned(ip) then
                   begin
                     bmp := TBitmap.Create;
                     try
                       case Odd(ud.inputFileNo) of
                         True:  case ud.exptNo of
                                  1: bmp.LoadFromResourceName(HINSTANCE, 'odd');
                                  2: bmp.LoadFromResourceName(HINSTANCE, 'exp_2_odd');
                                end;
                         False: case ud.exptNo of
                                  1: bmp.LoadFromResourceName(HINSTANCE, 'even');
                                  2: bmp.LoadFromResourceName(HINSTANCE, 'exp_2_even');
                         end;
                       end;
                       ip.Image.Picture.Bitmap := bmp;
                     finally
                       bmp.Free;
                     end;
                   end;
               end;
    else
      ;
  end;
end;

procedure TPageWizard.NextButtonClick(Sender: TObject);
begin
  if not Assigned(PageControl.ActivePage) then Exit;
  NextButton.Enabled := False;
  case TWizSheet(PageControl.ActivePage).PanelKind of
    pkGetInfo: begin
                 NextButton.Enabled := True;
                 NextButton.Caption := rsStartExperiment;
                 AdvancePage;
               end;
    pkInstructions: begin
                      AdvancePage;
                      Assert(TWizSheet(PageControl.ActivePage).PanelKind = pkExperiment, 'Programming page navigation error');
                      NextButton.Caption := rsContinue;
                      NextButton.Enabled := True;
                      RunExperiment;
                    end;
    pkExperiment, pkFeedback: ;
    else
      AdvancePage;
  end;
end;

procedure TPageWizard.QuitButtonClick(Sender: TObject);
var
  pk: EPanelKind;
begin
  if Assigned(OnQuit) and Assigned(PageControl.ActivePage) then
    begin
      pk := TWizSheet(PageControl.ActivePage).PanelKind;
      OnQuit(pk <> pkExperiment);
    end;
end;

{$IfDef applemainmenu}
procedure TPageWizard.InitAppleMenu;
var
  mm    : TMainMenu;
  apple : TMenuItem;
  abt   : TMenuItem;
  i     : integer;
const
  APPLE_LOGO = #$ef#$a3#$bf;
begin
  mm := Nil;
  for i := 0 to ComponentCount-1 do
    if Components[i] is TMainMenu then
      mm := TMainMenu(Components[i]);
  if not Assigned(mm) then begin
    mm := TMainMenu.Create(Self);
    mm.Parent:=Self;
  end;
  if not Assigned(mm) then Exit;

  apple := Nil;
  for i := 0 to mm.Items.Count - 1 do
    if mm.Items[i].Caption = APPLE_LOGO then begin
      apple := mm.items[i];
      Break;
    end;
  if not Assigned(apple) then begin
    apple := TMenuItem.Create(mm);
    apple.Caption := APPLE_LOGO;
    mm.Items.Insert(0, apple);
  end;

  abt := TMenuItem.Create(mm);
  abt.Caption := rsAbout;
  abt.OnClick := @AboutButtonClick;
  apple.Insert(0,abt);
end;
{$EndIf}

procedure TPageWizard.SetParent(NewParent: TWinControl);
begin
  inherited SetParent(NewParent);
  if Assigned(NewParent) then
    WordMap.Invalidate;
end;
{%endregion TPageWizard}

{%region TWizSheet}
constructor TWizSheet.Create(TheOwner: TPageWizard; const aTitle: String;
  aTitleStyle: TFontStyles; aPanelKind: EPanelKind);
begin
  inherited Create(TheOwner);

  FWizard := TheOwner;
  Color := TheOwner.Color;
  FPanelKind := aPanelKind;

  TitlePanel := TPanel.Create(Self);
  TitlePanel.Align := alTop;
  TitlePanel.Height := 22;
  TitlePanel.ParentColor := True;
  TitlePanel.ParentFont := True;
  TitlePanel.BevelOuter := bvNone;
  TitlePanel.Caption := aTitle;
  TitlePanel.Font.Style := aTitleStyle;
  TitlePanel.Parent := Self;

  case aPanelKind of
    pkIntro: Panel := TIntroPanel.Create(Self);
    pkConsent: Panel := TConsentPanel.Create(Self);
    pkUniqueID: Panel := TUniqueIDPanel.Create(Self);
    pkGetInfo: Panel := TGetInfoPanel.Create(Self);
    pkInstructions: Panel := TInstructionPanel.Create(Self);
    pkExperiment: Panel := TExperimentPanel.Create(Self);
    pkFeedback: Panel := TFeedbackPanel.Create(Self);
  end;

  Panel.Parent := Self;
end;
{%endregion TWizSheet}

{%region TFeedbackPanel}
constructor TFeedbackPanel.Create(TheOwner: TWizSheet);
begin
  inherited Create(TheOwner);

  TitleLabel := TLabel.Create(Self);
  ThanksLabel := TLabel.Create(Self);
  UploadLabel := TLabel.Create(Self);
  QuestionnaireLabel := TLabel.Create(Self);
  OpenFileButton := TButton.Create(Self);
  FQuURL := '';

  with TitleLabel do
    begin
      Caption := rsExperimentTerminated;
      Font.Height := -17;
      AnchorSideTop.Control := Self;
      AnchorSideLeft.Control := Self;
      AnchorSideLeft.Side := asrCenter;
      BorderSpacing.Around := Margin;
      WordWrap := True;
      Parent := Self;
    end;

  with ThanksLabel do
    begin
      Caption := rsManyThanks;
      Font.Height := -19;
      BorderSpacing.Top := Margin*2;
      BorderSpacing.Bottom := Margin*2;
      AnchorSideLeft.Control := Self;
      AnchorSideLeft.Side := asrCenter;
      AnchorSideTop.Control := TitleLabel;
      AnchorSideTop.Side := asrBottom;
      Parent := Self;
    end;

  with UploadLabel do
    begin
      AnchorSideLeft.Control := Self;
      AnchorSideLeft.Side := asrCenter;
      AnchorSideTop.Control := ThanksLabel;
      AnchorSideTop.Side := asrBottom;
      Cursor := crHandPoint;
      BorderSpacing.Top := Margin;
      Caption := rsUploadData;
      Font.Color := clHighlight;
      Font.Height := -15;
      Font.Style := [fsUnderline];
      OnClick := @UploadLabelClick;
      Parent := Self;
    end;

  with OpenFileButton do
    begin
      AnchorSideLeft.Control := Self;
      AnchorSideLeft.Side := asrCenter;
      AnchorSideTop.Control := UploadLabel;
      AnchorSideTop.Side := asrBottom;
      AutoSize := True;
      BorderSpacing.Top := Margin;
      Caption := rsOpenResults;
      OnClick := @ButtonClick;
      TabOrder := 0;
      Parent := Self;
    end;

  with QuestionnaireLabel do
    begin
      AnchorSideLeft.Control := Self;
      AnchorSideLeft.Side := asrCenter;
      AnchorSideTop.Control := OpenFileButton;
      AnchorSideTop.Side := asrBottom;
      Cursor := crHandPoint;
      BorderSpacing.Around := Margin;
      Caption := rsCompleteQuestionnaire;
      Font.Color := clHighlight;
      Font.Height := -15;
      Font.Style := [fsUnderline];
      OnClick := @QuestionnaireLabelClick;
      Parent := Self;
    end;
end;

procedure TFeedbackPanel.AdaptForExperimentResult(aResult: Integer);
begin
  case aResult of
    0: begin
         TitleLabel.Caption := Format(rsExperimentTerminated,
               [ExtractFilePath(ud.OutputFilename), ExtractFileName(ud.outputFilename)]);
         OpenFileButton.Enabled := FileExists(ud.OutputFilename);
         case (ud.age.ToInteger < 18) of
           True:  FQuURL := rsYoungQuestionnaireURL;
           False: FQuURL := rsQuestionnaireURL;
         end;
       end;
    else
      TitleLabel.Caption := Format(rsUserDidNotFinish, [ud.participantID]);
      QuestionnaireLabel.Enabled := False;
      QuestionnaireLabel.OnClick := Nil;
      OpenFileButton.Enabled := False;
      UploadLabel.Enabled := False;
      UploadLabel.OnClick := Nil;
      ThanksLabel.Caption := '';
  end;
end;

function TFeedbackPanel.CanMoveNext: Boolean;
begin
  Result := True;
end;

procedure TFeedbackPanel.ButtonClick(Sender: TObject);
begin
  OpenDocument(ud.OutputFilename);
end;

procedure TFeedbackPanel.QuestionnaireLabelClick(Sender: TObject);
begin
  OpenURL(FQuURL);
end;

procedure TFeedbackPanel.UploadLabelClick(Sender: TObject);
begin
  OpenURL(rsUploadURL);
end;
{%endregion TFeedbackPanel}

{%region TInstructionPanel}
constructor TInstructionPanel.Create(TheOwner: TWizSheet);
begin
  inherited Create(TheOwner);
  Image := TImage.Create(Self);
  with Image do begin
    Align := alClient;
    Proportional := True;
    Stretch := True;
    Parent := Self;
  end;
  DoNotifyWizardOfMoveNext;
end;

function TInstructionPanel.CanMoveNext: Boolean;
begin
  Result := True;
end;
{%endregion TInstructionPanel}

{%region TGetInfoPanel}
constructor TGetInfoPanel.Create(TheOwner: TWizSheet);
begin
  inherited Create(TheOwner);
  AgeEdit := TSpinEdit.Create(Self);
  AgeGB := TGroupBox.Create(Self);
  ChooseExperimentCombo := TComboBox.Create(Self);
  ChooseExperimentGB := TGroupBox.Create(Self);
  DescriptionMemo := TMemo.Create(Self);
  DisplayLabel := TLabel.Create(Self);
  ExperimentGB := TGroupBox.Create(Self);
  HandednessRG := TRadioGroup.Create(Self);
  InputFileGB := TGroupBox.Create(Self);
  ParticipantGB := TGroupBox.Create(Self);
  RandomRB := TRadioButton.Create(Self);
  SessionEdit := TSpinEdit.Create(Self);
  SessionGB := TGroupBox.Create(Self);
  SexRG := TRadioGroup.Create(Self);
  SpecificEdit := TSpinEdit.Create(Self);
  SpecificRB := TRadioButton.Create(Self);
  ProgressCB := TCheckBox.Create(Self);

  with ParticipantGB do begin
    AnchorSideTop.Control := Self;
    AnchorSideLeft.Control := Self;
    BorderSpacing.Left := Margin;
    Caption := ' ';
    AutoSize := True;
    TabOrder := 0;
    Parent := Self;
  end;

  with SexRG do begin
    AnchorSideTop.Control := ParticipantGB;
    AnchorSideLeft.Control := ParticipantGB;
    BorderSpacing.Around := Margin;
    AutoSize := True;
    Caption := rsSex;
    Items.Text := rsFemaleMale;
    TabOrder := 0;
    OnSelectionChanged := @SexSelectionChanged;
    Parent := ParticipantGB;
  end;

  with HandednessRG do begin
    AnchorSideTop.Control := ParticipantGB;
    AnchorSideLeft.Control := SexRG;
    AnchorSideLeft.Side := asrBottom;
    AutoSize := True;
    BorderSpacing.Around := Margin;
    Caption := rsHandedness;
    Items.Text := rsLefthanded;
    TabOrder := 1;
    OnSelectionChanged := @HandednessSelectionChanged;
    Parent := ParticipantGB;
  end;

  with AgeGB do begin
    AnchorSideTop.Control := ParticipantGB;
    AnchorSideLeft.Control := HandednessRG;
    AnchorSideLeft.Side := asrRight;
    BorderSpacing.Around := Margin;
    Caption := rsAge;
    AutoSize := True;
    TabOrder := 2;
    Parent := ParticipantGB;
  end;

  with AgeEdit do begin
    MinValue := 6;
    MaxValue := 100;
    Value := 20;
    AnchorSideTop.Control := AgeGB;
    AnchorSideLeft.Control := AgeGB;
    BorderSpacing.Around := 10;
    Parent := AgeGB;
  end;

  with ProgressCB do begin
    AnchorSideLeft.Control := ParticipantGB;
    AnchorSideTop.Control := SexRG;
    AnchorSideTop.Side := asrBottom;
    BorderSpacing.Around := Margin;
    Caption := rsShowProgress;
    Checked := True;
    TabOrder := 3;
    Parent := ParticipantGB;
  end;

  with ExperimentGB do begin
    AnchorSideTop.Control := Self;
    AnchorSideLeft.Control := ParticipantGB;
    AnchorSideLeft.Side := asrRight;
    BorderSpacing.Right := Margin;
    BorderSpacing.Top := 5;
    BorderSpacing.Left := DblMargin;
    BorderSpacing.Bottom := 10;
    Caption := rsExperimentDetails;
    AutoSize := True;
    TabOrder := 1;
    Parent := Self;
  end;

  with InputFileGB do begin
    AnchorSideTop.Control := ExperimentGB;
    AnchorSideLeft.Control := ExperimentGB;
    BorderSpacing.Around := Margin;
    AutoSize := True;
    Caption := rsInputFile;
    TabOrder := 1;
    Parent := ExperimentGB;
  end;

  with RandomRB do begin
    Caption := rsRandom;
    Checked := True;
    OnChange := @RandomSpecificChange;
    AnchorSideLeft.Control := InputFileGB;
    AnchorSideTop.Control := InputFileGB;
    BorderSpacing.Left := Margin;
    BorderSpacing.Top := 4;
    TabOrder := 0;
    Parent := InputFileGB;
  end;

  with SpecificRB do begin
    Caption := rsSpecific;
    OnChange := @RandomSpecificChange;
    AnchorSideLeft.Control := RandomRB;
    AnchorSideTop.Control := RandomRB;
    AnchorSideTop.Side := asrBottom;
    BorderSpacing.Top := 4;
    TabOrder := 1;
    Parent := InputFileGB;
  end;

  with SpecificEdit do begin
    AnchorSideLeft.Control := SpecificRB;
    AnchorSideLeft.Side := asrRight;
    AnchorSideTop.Control := SpecificRB;
    AnchorSideTop.Side := asrCenter;
    BorderSpacing.Left := 5;
    BorderSpacing.Right := Margin;
    BorderSpacing.Bottom := 10;
    Enabled := False;
    MinValue := 1;
    MaxValue := 32;
    TabOrder := 2;
    Parent := InputFileGB;
  end;

  with SessionGB do begin
    AnchorSideTop.Control := InputFileGB;
    AnchorSideLeft.Control := InputFileGB;
    AnchorSideLeft.Side := asrRight;
    BorderSpacing.Left := Margin;
    BorderSpacing.Right := Margin;
    AutoSize := True;
    Caption := rsSession;
    TabOrder := 3;
    Parent := ExperimentGB;
  end;

  with SessionEdit do begin
    AnchorSideLeft.Control := SessionGB;
    AnchorSideTop.Control := SessionGB;
    BorderSpacing.Left := Margin;
    BorderSpacing.Around := Margin;
    MinValue := 1;
    MaxValue := 10;
    TabOrder := 0;
    Parent := SessionGB;
  end;

  with DisplayLabel do begin
    AnchorSideTop.Control := InputFileGB;
    AnchorSideTop.Side := asrBottom;
    AnchorSideLeft.Control := ExperimentGB;
    BorderSpacing.Left := Margin;
    BorderSpacing.Right := Margin;
    BorderSpacing.Top := 10;
    BorderSpacing.Bottom := 10;
    Caption := rsDisplay + ud.monitorDescription;
    Parent := ExperimentGB;
  end;

  with ChooseExperimentGB do begin
    Anchors := [akTop, akLeft, akRight, akBottom];
    AnchorSideLeft.Control := ParticipantGB;
    AnchorSideTop.Control := ExperimentGB;
    AnchorSideTop.Side := asrBottom;
    AnchorSideRight.Control := Self;
    AnchorSideRight.Side := asrBottom;
    AnchorSideBottom.Control := Self;
    AnchorSideBottom.Side := asrBottom;
    BorderSpacing.Top := 10;
    BorderSpacing.Right := Margin;
    Caption := rsChooseExperimentLong;
    TabOrder := 2;
    Parent := Self;
  end;

  with ChooseExperimentCombo do begin
    AnchorSideLeft.Control := ChooseExperimentGB;
    AnchorSideTop.Control := ChooseExperimentGB;
    BorderSpacing.Top := 10;
    Constraints.MinWidth := 450;
    TabOrder := 0;
    Text := rsChooseExperiment;
    Items.Text := rsExperimentItems;
    OnChange := @ExperimentChange;
    Parent := ChooseExperimentGB;
  end;

  with DescriptionMemo do begin
    Anchors := [akTop, akLeft, akRight, akBottom];
    AnchorSideLeft.Control := ChooseExperimentCombo;
    AnchorSideTop.Control := ChooseExperimentCombo;
    AnchorSideTop.Side := asrBottom;
    AnchorSideRight.Control := ChooseExperimentGB;
    AnchorSideRight.Side := asrBottom;
    AnchorSideBottom.Control := ChooseExperimentGB;
    AnchorSideBottom.Side := asrBottom;
    BorderSpacing.Top := 5;
    BorderSpacing.Right := Margin;
    ScrollBars := ssAutoBoth;
    TabOrder := 1;
    Parent := ChooseExperimentGB;
  end;

end;

function TGetInfoPanel.CanMoveNext: Boolean;
begin
  Result := (SexRG.ItemIndex > -1) and (HandednessRG.ItemIndex > -1) and
            (RandomRB.Checked or SpecificRB.Checked) and
            (ChooseExperimentCombo.ItemIndex > -1);
end;

procedure TGetInfoPanel.ExperimentChange(Sender: TObject);
begin
  case ChooseExperimentCombo.ItemIndex of
    -1: DescriptionMemo.Clear;
    0: DescriptionMemo.Lines.Text := rsExp01WMCapacity;
    1: DescriptionMemo.Lines.Text := rsExp02WMLoadAndDistractorProcessing;
  end;
  DoNotifyWizardOfMoveNext;
end;

procedure TGetInfoPanel.HandednessSelectionChanged(Sender: TObject);
begin
  DoNotifyWizardOfMoveNext;
end;

procedure TGetInfoPanel.RandomSpecificChange(Sender: TObject);
begin
  SpecificEdit.Enabled := SpecificRB.Checked;
  DoNotifyWizardOfMoveNext;
end;

procedure TGetInfoPanel.SexSelectionChanged(Sender: TObject);
begin
  DoNotifyWizardOfMoveNext;
end;
{%endregion TGetInfoPanel}

{%region TUniqueIDPanel}
constructor TUniqueIDPanel.Create(TheOwner: TWizSheet);
begin
  inherited Create(TheOwner);

  DayRG := TRadioGroup.Create(Self);
  FathersInitialRG := TRadioGroup.Create(Self);
  YourInitialRG := TRadioGroup.Create(Self);
  IDLabel := TLabel.Create(Self);
  InstructionLabel := TLabel.Create(Self);
  MonthRG := TRadioGroup.Create(Self);
  MothersInitialRG := TRadioGroup.Create(Self);

  with InstructionLabel do begin
    AnchorSideLeft.Control := Self;
    AnchorSideLeft.Side := asrCenter;
    AnchorSideTop.Control := Self;
    Caption := rsCreateID;
    Font.Height := -13;
    ParentColor := False;
    ParentFont := False;
    WordWrap := True;
    Parent := Self;
  end;

  with YourInitialRG do begin
    AnchorSideLeft.Control := Self;
    AnchorSideLeft.Side := asrCenter;
    AnchorSideTop.Control := InstructionLabel;
    AnchorSideTop.Side := asrBottom;
    AutoFill := True;
    AutoSize := True;
    BorderSpacing.Top := 12;
    Caption := rsYourInitial;
    ChildSizing.LeftRightSpacing := 10;
    ChildSizing.EnlargeHorizontal := crsHomogenousChildResize;
    ChildSizing.EnlargeVertical := crsHomogenousChildResize;
    ChildSizing.ShrinkHorizontal := crsScaleChilds;
    ChildSizing.ShrinkVertical := crsScaleChilds;
    ChildSizing.Layout := cclLeftToRightThenTopToBottom;
    ChildSizing.ControlsPerLine := 13;
    Columns := 13;
    Items.Text := rsAtoZ;
    OnSelectionChanged := @OnRGChange;
    TabOrder := 0;
    Parent := Self;
  end;

  with MothersInitialRG do begin
    AnchorSideLeft.Control := Self;
    AnchorSideLeft.Side := asrCenter;
    AnchorSideTop.Control := YourInitialRG;
    AnchorSideTop.Side := asrBottom;
    AutoFill := True;
    AutoSize := True;
    BorderSpacing.Top := 7;
    Caption := rsMothersInitial;
    ChildSizing.LeftRightSpacing := 10;
    ChildSizing.EnlargeHorizontal := crsHomogenousChildResize;
    ChildSizing.EnlargeVertical := crsHomogenousChildResize;
    ChildSizing.ShrinkHorizontal := crsScaleChilds;
    ChildSizing.ShrinkVertical := crsScaleChilds;
    ChildSizing.Layout := cclLeftToRightThenTopToBottom;
    ChildSizing.ControlsPerLine := 13;
    Columns := 13;
    Items.Text := rsAtoZ;
    OnSelectionChanged := @OnRGChange;
    TabOrder := 1;
    Parent := Self;
  end;

  with FathersInitialRG do begin
    AnchorSideLeft.Control := Self;
    AnchorSideLeft.Side := asrCenter;
    AnchorSideTop.Control := MothersInitialRG;
    AnchorSideTop.Side := asrBottom;
    AutoFill := True;
    AutoSize := True;
    BorderSpacing.Top := 7;
    Caption := rsFathersInitial;
    ChildSizing.LeftRightSpacing := 10;
    ChildSizing.EnlargeHorizontal := crsHomogenousChildResize;
    ChildSizing.EnlargeVertical := crsHomogenousChildResize;
    ChildSizing.ShrinkHorizontal := crsScaleChilds;
    ChildSizing.ShrinkVertical := crsScaleChilds;
    ChildSizing.Layout := cclLeftToRightThenTopToBottom;
    ChildSizing.ControlsPerLine := 13;
    Columns := 13;
    Items.Text := rsAtoZ;
    OnSelectionChanged := @OnRGChange;
    TabOrder := 2;
    Parent := Self;
  end;

  with DayRG do begin
    AnchorSideLeft.Control := Self;
    AnchorSideLeft.Side := asrCenter;
    AnchorSideTop.Control := FathersInitialRG;
    AnchorSideTop.Side := asrBottom;
    AutoFill := True;
    AutoSize := True;
    BorderSpacing.Top := 7;
    Caption := rsDayOfBirth;
    ChildSizing.LeftRightSpacing := 10;
    ChildSizing.EnlargeHorizontal := crsHomogenousChildResize;
    ChildSizing.EnlargeVertical := crsHomogenousChildResize;
    ChildSizing.ShrinkHorizontal := crsScaleChilds;
    ChildSizing.ShrinkVertical := crsScaleChilds;
    ChildSizing.Layout := cclLeftToRightThenTopToBottom;
    ChildSizing.ControlsPerLine := 13;
    Columns := 13;
    Items.Text := rs1To31;
    OnSelectionChanged := @OnRGChange;
    OnClick := @DayMonthRGClick;
    TabOrder := 3;
    Parent := Self;
  end;

  with MonthRG do begin
    AnchorSideLeft.Control := Self;
    AnchorSideLeft.Side := asrCenter;
    AnchorSideTop.Control := DayRG;
    AnchorSideTop.Side := asrBottom;
    AutoFill := True;
    AutoSize := True;
    BorderSpacing.Top := 7;
    Caption := rsMonthOfBirth;
    ChildSizing.LeftRightSpacing := 10;
    ChildSizing.EnlargeHorizontal := crsHomogenousChildResize;
    ChildSizing.EnlargeVertical := crsHomogenousChildResize;
    ChildSizing.ShrinkHorizontal := crsScaleChilds;
    ChildSizing.ShrinkVertical := crsScaleChilds;
    ChildSizing.Layout := cclLeftToRightThenTopToBottom;
    ChildSizing.ControlsPerLine := 13;
    Columns := 13;
    Items.Text := rs1To12;
    OnSelectionChanged := @OnRGChange;
    OnClick := @DayMonthRGClick;
    TabOrder := 4;
    Parent := Self;
  end;

  with IDLabel do begin
    AnchorSideTop.Control := MonthRG;
    AnchorSideTop.Side := asrBottom;
    AnchorSideLeft.Control := Self;
    AnchorSideLeft.Side := asrCenter;
    BorderSpacing.Top := Margin;
    Caption := rsNoSelection;
    Font.Height := -16;
    Font.Style := [fsBold];
    ParentColor := False;
    ParentFont := False;
    Parent := Self;
  end;
end;

function TUniqueIDPanel.CanMoveNext: Boolean;
begin
  Result := fiB and miB and yiB and daB and moB and dayMonthB;
end;

procedure TUniqueIDPanel.DayMonthRGClick(Sender: TObject);
begin
  case DayRG.ItemIndex of
    -1: dayMonthB := False;
    0..28: dayMonthB := MonthRG.ItemIndex > -1; // days 1-29
    29: begin
          dayMonthB := (MonthRG.ItemIndex in [0 ,2..11]); // day 30
          if not dayMonthB and (MonthRG.ItemIndex = 1) then
            ShowMessage(rsFebDoesNotHave);
        end;
    30: begin
          dayMonthB := MonthRG.ItemIndex in [0, 2, 4, 6, 7, 9, 11]; // day 31
          if not dayMonthB and (MonthRG.ItemIndex > -1) then
            ShowMessageFmt(rsSDoesNotHave,[FormatSettings.LongMonthNames[Succ(MonthRG.ItemIndex)]]);
        end;
  end;
  DoNotifyWizardOfMoveNext;
end;

function TUniqueIDPanel.GetCaption: String;
begin
  Result := '';
  if yiB then Result := Result + YourInitialRG.Items[yi];
  if miB then Result := Result + MothersInitialRG.Items[mi];
  if fiB then Result := Result + FathersInitialRG.Items[fi];
  if daB then Result := Result + DayRG.Items[da];
  if moB then Result := Result + MonthRG.Items[mo];
end;

procedure TUniqueIDPanel.OnRGChange(Sender: TObject);
begin
  yi := YourInitialRG.ItemIndex;
  mi := MothersInitialRG.ItemIndex;
  fi := FathersInitialRG.ItemIndex;
  da := DayRG.ItemIndex;
  mo := MonthRG.ItemIndex;
  yiB := yi > -1;
  miB := mi > -1;
  fiB := fi > -1;
  daB := da > -1;
  moB := mo > -1;

  IDLabel.Caption := GetCaption;

  DoNotifyWizardOfMoveNext;
end;
{%endregion TUniqueIDPanel}

{%region TConsentPanel}
constructor TConsentPanel.Create(TheOwner: TWizSheet);
begin
  inherited Create(TheOwner);

  EscCheckBox := TCheckBox.Create(Self);
  EscLabel := TLabel.Create(Self);
  ReadCheckBox := TCheckBox.Create(Self);
  ReadLabel := TLabel.Create(Self);
  HTMLPanel := TIpHtmlPanel.Create(Self);
  WithdrawCheckBox := TCheckBox.Create(Self);
  WithdrawLabel := TLabel.Create(Self);

  with HTMLPanel do begin
    BorderSpacing.Around := Margin;
    Anchors := [akLeft, akRight, akTop];
    AnchorSideLeft.Control := Self;
    AnchorSideTop.Control := Self;
    AnchorSideRight.Control := Self;
    AnchorSideRight.Side := asrRight;
    Constraints.MinHeight := 270;
    Constraints.MaxHeight := 270;
    DefaultFontSize := 10;
    ParentColor := True;
    SetHtmlFromStr(rsConsent);
    OnHotClick := @HTMLPanelHotClick;
    Parent := Self;
  end;

  with ReadCheckBox do begin
    AnchorSideLeft.Control := Self;
    AnchorSideTop.Control := ReadLabel;
    AnchorSideTop.Side := asrCenter;
    BorderSpacing.Left := 14;
    BorderSpacing.Right := 4;
    OnChange := @CheckBoxChange;
    TabOrder := 0;
    Parent := Self;
  end;

  with ReadLabel do begin
    AnchorSideLeft.Control := ReadCheckBox;
    AnchorSideLeft.Side := asrBottom;
    AnchorSideTop.Control := HTMLPanel;
    AnchorSideTop.Side := asrBottom;
    Caption := rsIHaveReadAboveText;
    WordWrap := True;
    OnClick := @ReadLabelClick;
    Parent := Self;
  end;

  with EscCheckBox do begin
    AnchorSideLeft.Control := ReadCheckBox;
    AnchorSideTop.Control := EscLabel;
    AnchorSideTop.Side := asrCenter;
    BorderSpacing.Right := 4;
    OnChange := @CheckBoxChange;
    TabOrder := 1;
    Parent := Self;
  end;

  with EscLabel do begin
    AnchorSideLeft.Control := EscCheckBox;
    AnchorSideLeft.Side := asrBottom;
    AnchorSideTop.Control := ReadLabel;
    AnchorSideTop.Side := asrBottom;
    BorderSpacing.Top := 8;
    Caption := rsIAmAware;
    OnClick := @EscLabelClick;
    Parent := Self;
  end;

  with WithdrawCheckBox do begin
    AnchorSideLeft.Control := ReadCheckBox;
    AnchorSideTop.Control := WithdrawLabel;
    AnchorSideTop.Side := asrCenter;
    BorderSpacing.Right := 4;
    OnChange := @CheckBoxChange;
    TabOrder := 2;
    Parent := Self;
  end;

  with WithdrawLabel do begin
    AnchorSideLeft.Control := WithdrawCheckBox;
    AnchorSideLeft.Side := asrRight;
    AnchorSideTop.Control := EscLabel;
    AnchorSideTop.Side := asrBottom;
    BorderSpacing.Top := 8;
    BorderSpacing.Bottom := 8;
    Caption := rsIUnderstandICanWithdraw;
    WordWrap := True;
    OnClick := @WithdrawLabelClick;
    Parent := Self;
  end;
end;

function TConsentPanel.CanMoveNext: Boolean;
begin
  Result := ReadCheckBox.Checked and EscCheckBox.Checked and WithdrawCheckBox.Checked;
end;

procedure TConsentPanel.HTMLPanelHotClick(Sender: TObject);
begin
  if HTMLPanel.HotURL <> '' then
    OpenURL(HTMLPanel.HotURL);
end;

procedure TConsentPanel.CheckBoxChange(Sender: TObject);
begin
  DoNotifyWizardOfMoveNext;
end;

procedure TConsentPanel.EscLabelClick(Sender: TObject);
begin
  EscCheckBox.Checked := not EscCheckBox.Checked;
  DoNotifyWizardOfMoveNext;
end;

procedure TConsentPanel.ReadLabelClick(Sender: TObject);
begin
  ReadCheckBox.Checked := not ReadCheckBox.Checked;
  DoNotifyWizardOfMoveNext;
end;

procedure TConsentPanel.WithdrawLabelClick(Sender: TObject);
begin
  WithdrawCheckBox.Checked := not WithdrawCheckBox.Checked;
  DoNotifyWizardOfMoveNext;
end;
{%endregion TConsentPanel}

{%region TIntroPanel}
constructor TIntroPanel.Create(TheOwner: TWizSheet);
begin
  inherited Create(TheOwner);
  Memo := TMemo.Create(Self);
  Memo.BorderSpacing.Around := Margin shl 1;
  Memo.Align := alClient;
  Memo.ScrollBars := ssAutoBoth;
  Memo.Text := rsIntroText;
  Memo.TabStop := False;
  Memo.ReadOnly := True;
  Memo.Parent := Self;
  DoNotifyWizardOfMoveNext;
end;

function TIntroPanel.CanMoveNext: Boolean;
begin
  Result := True;
end;
{%endregion TIntroPanel}

{%region TBasePanel}
constructor TBasePanel.Create(TheOwner: TWizSheet);
begin
  inherited Create(TheOwner);
  FWizard := TheOwner.Wizard;
  BevelOuter := bvNone;
  Caption := '';
  Align := alClient;
end;

function TBasePanel.CanMoveNext: Boolean;
begin
  Result := False;
end;

procedure TBasePanel.DoNotifyWizardOfMoveNext;
begin
  FWizard.DoMoveNext(CanMoveNext);
end;

{%endregion TBasePanel}

end.

