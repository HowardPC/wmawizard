unit uAboutForm;

{
  Working Memory Analyser (Wizard Version 1.0.3)

  An open source Psychology software to investigate both individual
  differences in working memory capacity and the effect of working
  memory load on visual distractibility

  Copyright © 2020-2021 by Phil Duke and Giorgio Fuggetta

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
}

{
Special Thanks to:

 - skalogryz (email: unspecified), additional developed, optimised and compiled the software for MacOS.
 - Howard Page-Clark (email: pageclarkhoward@gmail.com), refactored earlier code to make the experiment
   resource-based rather than file-based, and developed the wizard interface and data-processing modules.
}

{$Mode objfpc}{$H+}

interface

uses
  Classes,
  Forms, StdCtrls;

type

  TAboutForm = class(TForm)
    btnClose: TButton;
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown({%H-}Sender: TObject; var Key: Word; {%H-}Shift: TShiftState);
  end;

  procedure ShowAboutForm;

implementation

uses
  LCLType, Controls,
  uRStrings;

procedure ShowAboutForm;
var
  dlg: TAboutForm;
begin
  dlg := TAboutForm.Create(Nil);
  try
    dlg.ShowModal;
  finally
    dlg.Free;
  end;
end;

{$R *.lfm}

procedure TAboutForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    begin
      Key := 0;
      ModalResult := mrClose;
    end;
end;

procedure TAboutForm.FormCreate(Sender: TObject);
begin
  Memo1.Lines.Text := rsAboutInfo;
end;

end.

