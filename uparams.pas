unit uParams;

{
  Working Memory Analyser (Wizard Version 1.0.3)

  An open source Psychology software to investigate both individual
  differences in working memory capacity and the effect of working
  memory load on visual distractibility

  Copyright © 2020-2021 by Phil Duke and Giorgio Fuggetta

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
}

{
Special Thanks to:

 - skalogryz (email: unspecified), additional developed, optimised and compiled the software for MacOS.
 - Howard Page-Clark (email: pageclarkhoward@gmail.com), refactored earlier code to make the experiment
   resource-based rather than file-based, and developed the wizard interface and data-processing modules.
}

{$Mode ObjFPC}{$H+}
{$ModeSwitch advancedrecords}

interface

uses
  {$IfDef MSWindows}Windows, ShlObj,{$EndIf}
  Math, Classes, SysUtils, Types,
  sdl2, sdl2_ttf,
  edidMonitorUtils,
  uInpFiles, uRStrings;

const

  GlobalPollKey1: TSDL_KeyCode = SDLK_a;
  GlobalPollKey2: TSDL_KeyCode = SDLK_f;
  PiOver180 = Pi/180;
  PiOver8 = Pi/8;
  PiOver4 = Pi/4;
  TwoPi = Pi * 2;
  HalfPentagonAngle = ((5-2) * 180) / (5 * 2);
  PentagonSideToRadius = Sqrt(10) * Sqrt(5 + sqrt(5)) / 10;

  LastTrainingIdx = 15;
  MaxShapeColorIndex = 15;
  NTrialsBeforePause = 16;
  ResponseLeftButton = 1;
  ResponseRightButton = 2;
  ShapeScaleFactor = 1;
  RTconstantErrorMS = 49;
  Distance:                   Single = 57;
  DistancePoint:              Single = 57.01;
  BackgroundDiameterDeg:      Single = 18.3;       // the size of the "gray background"
  FixationDotDeg:             Single = 0.1;
  HalfBackgroundDiamDeg:      Single = 9.15;
  HalfPlaceholderDiameterDeg: Single = 1;
  HalfPlaceholdersDiamDeg:    Single = 7.1;
  ImageFeedbackDeg:           Single = 3.2;
  ImageSizeDeg:               Single = 1.3;
  PlaceholderDiameterDeg:     Single = 2;
  PlaceholderRadiusDeg:       Single = 0.69230769;
  PlaceholdersDiameterDeg:    Single = 14.2;       // "virtual" circle of where the 4 place holders and a target are drawn at
  S2SampleDiameterDeg:        Single = 4.1;        // "virtual" circle of where the samples are drawn
  ShapeSizeDeg:               Single = 1.3;
  MinimumTrainingAccuracy:    Single = 0.6;

  SDLColorWhite: TSDL_Color = (r: 255; g: 255; b: 255; a: 0);

  CorrectBMP = 33;
  IncorrectBMP = 34;

type

  TColourReal = record
    rByte, gByte, bByte: Byte;
    r, g, b: Real;
    function ToSDLColor(anA: Byte): TSDL_Color;
    function ToCSVStr: String;
  end;

  ExperimentTerminateException = class(Exception);

  TExtraInputData = record
    Factor: array[1..9] of string;
  end;

  TByteSet = set of Byte;

  TExperimentNoRange = MinExperimentNo..MaxExperimentNo;

  TInputDataFileNoRange = MinInputDataFileNo..MaxInputDataFileNo;

  Rxyz = record
    x, y, z: Single;
    procedure Rotate(theta: Single; axis: Char);
    procedure Init(anX, aY, aZ: Single);
  end;

  TSurfaceArr = array[0..IncorrectBMP] of PSDL_Surface;

const

  PauseTrialIndexes: TByteSet = [0, 16, 48, 80, 112, 144, 176, 208, 240];

  ExperimentNames: array[TExperimentNoRange] of String = (rsExperiment01, rsExperiment02);

  RealColorBlack: TColourReal = (rByte: 0;  gByte: 0;  bByte: 0; r: 0; g: 0; b: 0);
  PauseBackgroundCircleColour: TColourReal = (rByte: 18;  gByte: 18;  bByte: 18;  r: 18/255; g: 18/255; b: 18/255);
  RunBackgroundCircleColour: TColourReal   = (rByte: 38;  gByte: 38;  bByte: 38;  r: 38/255; g: 38/255; b: 38/255);
  FixationColour: TColourReal              = (rByte: 72;  gByte: 72;  bByte: 72;  r: 72/255; g: 72/255; b: 72/255);
  IncorrectFeedbackColour: TColourReal     = (rByte: 170; gByte: 0;   bByte: 0;  r: 170/255; g: 0;      b: 0);
  CorrectFeedbackColour: TColourReal       = (rByte: 0;   gByte: 101; bByte: 0;   r: 0;      g: 101/255; b: 0);
  PlaceholdersColour: TColourReal          = (rByte: 38;  gByte: 38;  bByte: 38;  r: 38/255; g: 38/255; b: 38/255);
  PlaceholdersColourE2: TColourReal        = (rByte: 72;  gByte: 72;  bByte: 72;  r: 72/255; g: 72/255; b: 72/255);
  Colours: array [0..15] of TColourReal = ((rByte: 72;  gByte: 72;  bByte: 72;  r: 72/255;   g: 72/255; b: 72/255),
                                           (rByte: 146; gByte: 0;  bByte: 0;    r: 146/255;  g: 0;      b: 0),
                                           (rByte: 0;   gByte: 85; bByte: 0;    r: 0;        g: 85/255; b: 0),
                                           (rByte: 0;   gByte: 0;  bByte: 255;  r: 0;        g: 0;      b: 1),
                                           (rByte: 74;  gByte: 74; bByte: 0;    r: 74/255;   g: 74/255; b: 0),
                                           (rByte: 130; gByte: 0;  bByte: 130;  r: 130/255;  g: 0;      b: 130/255),
                                           (rByte: 72;  gByte: 72; bByte: 72;   r: 72/255;   g: 72/255; b: 72/255),
                                           (rByte: 0;   gByte: 83; bByte: 83;   r: 0;        g: 83/255; b: 83/255),
                                           (rByte: 96;  gByte: 65; bByte: 0;    r: 96/255;   g: 65/255; b: 0),
                                           (rByte: 72;  gByte: 72; bByte: 72;   r: 72/255;   g: 72/255; b: 72/255),
                                           (rByte: 72;  gByte: 72; bByte: 72;   r: 72/255;   g: 72/255; b: 72/255),
                                           (rByte: 104; gByte: 0;  bByte: 200;  r: 104/255;  g: 0;      b: 200/255),
                                           (rByte: 85;  gByte: 85; bByte: 85;   r: 85/255;   g: 85/255; b: 85/255),
                                           (rByte: 0;   gByte: 0;  bByte: 0;    r:0; g:0; b:0),
                                           (rByte: 0;   gByte: 0;  bByte: 0;    r:0; g:0; b:0),
                                           (rByte: 0;   gByte: 0;  bByte: 0;    r:0; g:0; b:0) );

type

  RUserData = record
  private
    FDateStr: String;
    FDateTime: TDateTime;
    FHalfHeight: Integer;
    FHalfHeightCM: Single;
    FHalfWidth: Integer;
    FHalfWidthCM: Single;
    FHeightToHCMRatio: Single;
    FHoldersColor: TcolourReal;
    FOutputFilename: String;
    FReciprocalRefresh: Single;
    FShowS3Placeholders: Boolean;
    FTimeStr: String;
    FWidthToWCMRatio: Single;
    function GetExperimentName: String;
    function GetOutputFilename: String;
  public
    age: String;
    exptNo: TExperimentNoRange;
    handed: String;
    imageScaleHoriz: Single;
    imageScaleVert: Single;
    inputFileNo: TInputDataFileNoRange;
    monHeight: Integer;
    monHeightCM: Single;
    monitorDescription: String;
    monitorName: String;
    monRefresh: Single;
    monWidth: Integer;
    monWidthCM: Single;
    monXPos: Integer;
    monYPos: Integer;
    participantID: String;
    reportTrialPogress: Boolean;
    sessionNo: Integer;
    sex: String;
    procedure Init(const anAge, aHanded, aSex: String; anExptNo, anInputFNo, aSession: Integer; isRandom: Boolean=True; reportProgress: Boolean=True);
    procedure SetMonitor(aMonitor: TMonitor);
    property DateStr: String read FDateStr;
    property ExperimentName: String read GetExperimentName;
    property HalfHeight: Integer read FHalfHeight;
    property HalfHeightCM: Single read FHalfHeightCM;
    property HalfWidth: Integer read FHalfWidth;
    property HalfWidthCM: Single read FHalfWidthCM;
    property HeightToHCMRatio: Single read FHeightToHCMRatio;
    property HoldersColor: TColourReal read FHoldersColor;
    property OutputFilename: String read FOutputFilename;
    property ReciprocalRefresh: Single read FReciprocalRefresh;
    property ShowS3Placeholders: Boolean read FShowS3Placeholders;
    property TimeStr: String read FTimeStr;
    property WidthToWCMRatio: Single read FWidthToWCMRatio;
  end;

  RResponse = record
  private
    FCorrectResponse: Integer;
    FCumCorrectResp: Integer;
    function GetAccuracy: Single;
    procedure SetCorrectResponse(aValue: Integer);
  public
    blankOnsetTime,
    feedbackOnsetTime,
    isRuinedTrial,
    response,
    responseOnsetTime,
    rEventTime,
    rState,
    RTms,
    RTmsLessConstError,
    s1OnsetTime,
    s2OnsetTime,
    s3OnsetTime,
    s4OnsetTime,
    t1,
    TMSOnsetTime: Integer;
  public
    procedure ConsoleReport;
    procedure Init;
    procedure ReInit;
    property Accuracy: Single read GetAccuracy;
    property CorrectResponse: Integer read FCorrectResponse write SetCorrectResponse;
    property CumCorrectResp: Integer read FCumCorrectResp write FCumCorrectResp;
  end;

  function DegToCm(deg, distance: Single): Single;

  function LoadSurfaceArrayOK(out SurfaceArray: TSurfaceArr): Boolean;

  function SetupFonts(out fGen, fStim, fFeedBck: PTTF_Font): Integer;

  procedure GetCircleCoords(out Xarr, Yarr: TSingleDynArray; Radius: Single; Npoints: Integer);

  procedure ObjectLocation(out x, y: Single; cueRadiusCM: Single; cueQuadrant: Integer);

var
  ud: RUserData;
  experimentResult: Integer;

implementation

uses
  LazFileUtils;

function GetDocumentsDir: String;
{$IfDef MSWindows}
var
  ws: WideString;
  l: Integer;
begin
  SetLength(ws, MAX_PATH);
  if SHGetSpecialFolderPathW(0, @ws[1], CSIDL_PERSONAL, false) then
    begin
      l := StrLen(PWideChar(@ws[1]));
      SetLength(ws, l);
      Result := UTF8Encode(ws);
    end
  else
    Result := IncludeTrailingPathDelimiter(GetUserDir)+'Documents';
end;
{$Else}
begin
  Result := IncludeTrailingPathDelimiter(GetUserDir) + 'Documents';
end;
{$EndIf}

function DegToCm(deg, distance: Single): Single;
begin
  Result := deg * PiOver180 * distance;
end;

function LoadSurfaceArrayOK(out SurfaceArray: TSurfaceArr): Boolean;
type
  TBuf = array[1..7654] of Byte;
  TCBuf = array[1..48822] of Byte;
  TIBuf = array[1..48438] of Byte;
var
  j: Integer;
  rs: TResourceStream;
  buf: TBuf;
  cBuf: TCBuf;
  iBuf: TIBuf;
  f: file of TBuf;
  fc: file of TCBuf;
  fi: file of TIBuf;
  fn: String;
begin
  // This routine is a deadful hack, however it works. Whereas the preferred method via SDL_LoadBMP_RW() using RWOps and individual bitmap resources fails, and using an imagelist gives the bitmaps the wrong background colour
  SurfaceArray := Default(TSurfaceArr);
  rs := TResourceStream.Create(HINSTANCE, 'BMPS', PChar(RT_RCDATA)); // BMPs is a concatenation of the 33 bitmaps
  try
    for j := 0 to 32 do
      begin
        fn := GetTempFileName;
        rs.Read(buf{%H-}, SizeOf(buf));
        AssignFile(f, fn);
        Rewrite(f);
        Write(f, buf);
        CloseFile(f);
        SurfaceArray[j] := SDL_LoadBMP(PChar(fn));
        DeleteFile(fn);
        if not Assigned(SurfaceArray[j]) then
          Exit(False);
      end;
    rs.Free;
    rs := TResourceStream.Create(HINSTANCE, 'CORRINCORR', PChar(RT_RCDATA));
    for j := CorrectBMP to IncorrectBMP do
      begin
        case j of
          CorrectBMP:   begin
                          rs.Read(cBuf{%H-}, SizeOf(cBuf));
                          fn := GetTempFileName;
                          AssignFile(fc, fn);
                          Rewrite(fc);
                          Write(fc, cBuf);
                          CloseFile(fc);
                          SurfaceArray[j] := SDL_LoadBMP(PChar(fn));
                          DeleteFile(fn);
                          if not Assigned(SurfaceArray[j]) then
                            Exit(False);
                        end;
          IncorrectBMP: begin
                          rs.Read(iBuf{%H-}, SizeOf(iBuf));
                          fn := GetTempFileName;
                          AssignFile(fi, fn);
                          Rewrite(fi);
                          Write(fi, iBuf);
                          CloseFile(fi);
                          SurfaceArray[j] := SDL_LoadBMP(PChar(fn));
                          DeleteFile(fn);
                          if not Assigned(SurfaceArray[j]) then
                            Exit(False);
                        end;
        end;
      end;
    Result := True;
  finally
    rs.Free;
  end;
end;

function SetupFonts(out fGen, fStim, fFeedBck: PTTF_Font): Integer;

    function GetFontFromResource(const {%H-}ResName: String; aPtSize: Integer; out PFont: PTTF_Font): Boolean;
    var
      rs: TResourceStream;
      fn: String;
    begin
      rs := TResourceStream.Create(HINSTANCE, ResName, PChar(RT_RCDATA));
      try
        fn := GetTempFileName;
        PFont := Nil;
        rs.SaveToFile(fn);
        PFont := TTF_OpenFont(PChar(fn), aPtSize);
        DeleteFile(fn);
        Result := Assigned(PFont);
      finally
        rs.Free;
      end;
    end;

  begin
    fGen := Nil; fStim := Nil; fFeedBck := Nil;
    Result := TTF_Init;
    if Result <> 0 then
      Exit;
    if not GetFontFromResource(rsARIAL, Round(2 * ud.WidthToWCMRatio), fGen
      ) then
      Exit(-2);
    TTF_SetFontStyle(fGen, TTF_STYLE_NORMAL);
    if not GetFontFromResource(rsARIAL, Round(1.4 * ud.WidthToWCMRatio), fStim) then
      Exit(-3);
    TTF_SetFontStyle(fStim, TTF_STYLE_NORMAL);
    if not GetFontFromResource(rsARIAL, Round(3 * ud.WidthToWCMRatio), fFeedBck) then
      Exit(-4);
    TTF_SetFontStyle(fFeedBck, TTF_STYLE_NORMAL);
  end;

procedure GetCircleCoords(out Xarr, Yarr: TSingleDynArray; Radius: Single; Npoints: Integer);
  var
    i: Integer;
    sinTheta, cosTheta, fac: Single;
  begin
    Xarr := Nil; Yarr := Nil;
    SetLength(Xarr, Npoints);
    SetLength(Yarr, Npoints);
    fac := TwoPi/Npoints;
    for i := 0 to Npoints-1 do
      begin
        SinCos(i * fac, sinTheta, cosTheta);
        Xarr[i] := Radius * sinTheta;
        Yarr[i] := Radius * cosTheta;
      end;
  end;

procedure ObjectLocation(out x, y: Single; cueRadiusCM: Single; cueQuadrant: Integer);
var
  sinPi4, cosPi4: Single;
begin
  SinCos(PiOver4, sinPi4, cosPi4);
  case cueQuadrant of
    1: begin
         x := -cueRadiusCM * cosPi4;    // 45degrees "North West"
         y :=  cueRadiusCM * sinPi4;
       end;
    2: begin
         x :=  cueRadiusCM * cosPi4;
         y :=  cueRadiusCM * sinPi4;
       end;
    3: begin
         x :=  cueRadiusCM * cosPi4;
         y := -cueRadiusCM * sinPi4;
       end;
    4: begin
         x := -cueRadiusCM * cosPi4;
         y := -cueRadiusCM * sinPi4;
       end;
    5: begin  //special case of all quadrants
         x := 0;
         y := 0;
       end;
    else Assert(False, 'Programmer error in cueQuadrant parameter value: '+cueQuadrant.ToString);
  end;
end;

{%region TColourReal}
function TColourReal.ToSDLColor(anA: Byte): TSDL_Color;
begin
  Result := Default(TSDL_Color);
  Result.r := rByte;
  Result.g := gByte;
  Result.b := bByte;
  Result.a := anA;
end;

function TColourReal.ToCSVStr: String;
begin
  Result := '';
  Result := Result + rByte.ToString + ',' + gByte.ToString + ',' + bByte.ToString;
end;
{%endregion TColourReal}

{%region Rxyz}
procedure Rxyz.Rotate(theta: Single; axis: Char);
var
  cosTheta, sinTheta: Single;
begin
  SinCos(theta, sinTheta, cosTheta);
  case axis of
    'y','Y': begin
               x :=  x * cosTheta + z * sinTheta;
               z := -x * sinTheta + z * cosTheta;
             end;
    'x','X': begin
               y := y * cosTheta - z * sinTheta;
               z := y * sinTheta + z * cosTheta;
             end;
    'z','Z': begin
               x := x * costheta - y * sinTheta;
               y := x * sintheta + y * cosTheta;
             end;
    else Assert(False, 'Rxyz.Rotate: invalid axis parameter '+axis);
  end;
end;

procedure Rxyz.Init(anX, aY, aZ: Single);
begin
  x := anX; y := aY; z := aZ;
end;
{%endregion Rxyz}

{%region RUserData}
function RUserData.GetExperimentName: String;
begin
  Exit(ExperimentNames[exptNo]);
end;

function RUserData.GetOutputFilename: String;
var
  path: String;
  y, m, d, h, mn, s, ms: Word;
begin
  path := IncludeTrailingPathDelimiter(GetDocumentsDir) + ExperimentName;
  ForceDirectoriesUTF8(path);
  path := IncludeTrailingPathDelimiter(path);
  DecodeDate(FDateTime, y, m, d);
  DecodeTime(FDateTime, h, mn, s, ms);
  Result := path + Format('OutputData_%d_%d_%d_%d-%d-%d_%d_%d_%s.txt',
                          [y, m, d, h, mn, s, inputFileNo, sessionNo, participantID]);
end;

procedure RUserData.Init(const anAge, aHanded, aSex: String; anExptNo,
  anInputFNo, aSession: Integer; isRandom: Boolean; reportProgress: Boolean);
begin
  age := anAge;
  handed := aHanded;
  sex := aSex;
  case isRandom of
    True:  begin Randomize; inputFileNo := Succ(Random(MaxInputDataFileNo)); end;
    False: inputFileNo := anInputFNo;
  end;
  sessionNo := aSession;
  exptNo := anExptNo;
  reportTrialPogress := reportProgress;
  FDateTime := Now;
  FDateStr := DateToStr(FDateTime);
  FTimeStr := TimeToStr(FDateTime);
  FOutputFilename := GetOutputFilename;
  case exptNo of
    1: begin FHoldersColor := PlaceholdersColour;   FShowS3Placeholders := False; end;
    2: begin FHoldersColor := PlaceholdersColourE2; FShowS3Placeholders := True; end;
  end;
end;

procedure RUserData.SetMonitor(aMonitor: TMonitor);
begin
  Self := default(RUserData);
  monitorName := aMonitor.Name;
  monWidth := aMonitor.Resolution.cx;
  if monWidth < 1 then
    monWidth := 1920;
  FHalfWidth := monWidth shr 1;
  monHeight := aMonitor.Resolution.cy;
  if monHeight < 1 then
    monHeight := 1080;
  FHalfHeight := monHeight shr 1;
  monWidthCM := aMonitor.PhysSizeMm.cx / 10;
  if monWidthCM < 0.1 then
    monWidthCM := 34.4;
  FHalfWidthCM := monWidthCM / 2;
  monHeightCM := aMonitor.PhysSizeMm.cy / 10;
  if monHeightCM < 0.1 then
    monHeightCM := 19.3;
  FHalfHeightCM := monHeightCM / 2;
  FHeightToHCMRatio := monHeight/monHeightCM;
  FWidthToWCMRatio := monWidth/monWidthCM;
  monRefresh := aMonitor.Frequency;
  if monRefresh < 0.1 then
    monRefresh := 60;
  FReciprocalRefresh := 1 / monRefresh;
  monXPos := aMonitor.Bounds.Left;
  monYPos := aMonitor.Bounds.Top;
  imageScaleHoriz := monWidth / 1920;
  imageScaleVert := monHeight / 1080;
  monitorDescription := Format(' %s  (%d x %d)  %d Hz',[aMonitor.Name, aMonitor.Resolution.cx, aMonitor.Resolution.cy, Trunc(aMonitor.Frequency)]);
  if (aMonitor.PhysSizeMm.cx > 0) and (aMonitor.PhysSizeMm.cy > 0) then
    monitorDescription := monitorDescription + Format(' (%.1f cm x %.1f cm)', [aMonitor.PhysSizeMm.cx/10, aMonitor.PhysSizeMm.cy/10]);
end;
{%endregion RUserData}

{%region RResponse}
procedure RResponse.SetCorrectResponse(aValue: Integer);
begin
  FCorrectResponse := aValue;
  Inc(FCumCorrectResp, aValue);
end;

function RResponse.GetAccuracy: Single;
begin
  Result := CumCorrectResp / 16;
end;

procedure RResponse.Init;
begin
  FillWord(Self, SizeOf(Self) div 2, 65535);
  FCumCorrectResp := 0;
  isRuinedTrial := 0;
end;

procedure RResponse.ReInit;
var
  tmp: Integer;
begin
  tmp := FCumCorrectResp;
  Init;
  FCumCorrectResp := tmp;
  rEventTime := 0;
end;

procedure RResponse.ConsoleReport;
begin
  WriteLn('Response:',Response,', RTms:',RTms,', RTmsLessConstError:',RTmsLessConstError,', s1OnsetTime:',s1OnsetTime);
  WriteLn('s2OnsetTime:',s2OnsetTime,', s3OnsetTime:',s3OnsetTime,', TMSOnsetTime:',TMSOnsetTime,', s4OnsetTime:',s4OnsetTime);
  WriteLn('responseOnsetTime:',responseOnsetTime,', feedbackOnsetTime:',feedbackOnsetTime,', blankOnsetTime:',blankOnsetTime);
  WriteLn('isRuinedTrial:',isRuinedTrial,', CumCorrectResp:',CumCorrectResp,', CorrectResponse:',CorrectResponse);
  WriteLn('t1:',t1,', rState:',rState,', rEventTime:',rEventTime,', Accuracy=',Accuracy:2:1,LineEnding);
end;
{%endregion RResponse}

end.

