unit uWordMap;

{
  Working Memory Analyser (Wizard Version 1.0.3)

  An open source Psychology software to investigate both individual
  differences in working memory capacity and the effect of working
  memory load on visual distractibility

  Copyright © 2020-2021 by Phil Duke and Giorgio Fuggetta

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
}

{
Special Thanks to:

 - skalogryz (email: unspecified), additional developed, optimised and compiled the software for MacOS.
 - Howard Page-Clark (email: pageclarkhoward@gmail.com), refactored earlier code to make the experiment
   resource-based rather than file-based, and developed the wizard interface and data-processing modules.
}

{$Mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Types,
  Controls, Graphics;

type

TWordMap = class(TGraphicControl)
private
  const
    MinAcceptableWordHeight = 9;
private
  FConcat: String;
  FHighlightColor: TColor;
  FHighWidths: TIntegerDynArray;
  FItemIndex: Integer;
  FMaxTextHeight: Integer;
  FMinTopMargin: Integer;
  FNormalColor: TColor;
  FNormalWidths: TIntegerDynArray;
  FNormalWordHeight: Integer;
  FTotalPaintedWidth: Integer;
  FWords: TStringArray;
  FWordSpacing: Integer;
  function CalculatedWidthIsOK: Boolean;
  function GetHighItemIndex: Integer;
  function NeedToAdjustFontHeightToFit(out FontHeight: Integer): Boolean;
  procedure AdjustFontToFitText;
  procedure SetHighlightColor(aValue: TColor);
  procedure SetItemIndex(aValue: Integer);
  procedure SetMinTopMargin(aValue: Integer);
  procedure SetNormalColor(aValue: TColor);
  procedure SetWordSpacing(aValue: Integer);
protected
  procedure DoSetBounds(ALeft, ATop, AWidth, AHeight: integer); override;
  class function GetControlClassDefaultSize: TSize; override;
public
  constructor Create(anOwnerParent: TWinControl); reintroduce;
  procedure AddWord(const aWord: String);
  procedure Paint; override;
  property HighItemIndex: Integer read GetHighItemIndex;
  property HighlightColor: TColor read FHighlightColor write SetHighlightColor default clMenuText;
  property ItemIndex: Integer read FItemIndex write SetItemIndex;
  property MinTopMargin: Integer read FMinTopMargin write SetMinTopMargin default 4;
  property NormalColor: TColor read FNormalColor write SetNormalColor default clMenuHighlight;
  property Words: TStringArray read FWords;
  property WordSpacing: Integer read FWordSpacing write SetWordSpacing default 14;
end;

implementation

uses
  LCLType, LCLIntf;

{%region TWordMap}
constructor TWordMap.Create(anOwnerParent: TWinControl);
begin
  inherited Create(anOwnerParent);

  FConcat := '';
  FMinTopMargin := 4;
  FWordSpacing := 14;
  FHighlightColor := clMenuText;
  FNormalColor := clMenuHighlight;
  FItemIndex := -1;
  ControlStyle := [csNoStdEvents, csReplicatable];
  ParentFont := True;
  ParentColor := True;
  with GetControlClassDefaultSize do
    SetInitialBounds(0, 0 , cx, cy);
  Align := alTop;
  parent := anOwnerParent;
end;

procedure TWordMap.AddWord(const aWord: String);
begin
  Assert(Assigned(Parent),'You must parent this control before adding words');
  if aWord <> '' then
    begin
      FConcat := FConcat + aWord;
      SetLength(FWords, Succ(Length(FWords)));
      FWords[High(FWords)] := aWord;
      SetLength(FNormalWidths, Length(FWords));
      SetLength(FHighWidths, Length(FWords));
      if not CalculatedWidthIsOK then
        AdjustFontToFitText;
    end;
end;

procedure TWordMap.AdjustFontToFitText;
var
  newHeight: Integer;
  needsAdjusting: Boolean;
begin
  needsAdjusting := NeedToAdjustFontHeightToFit(newHeight);
  if needsAdjusting and (newHeight <> Font.Height) then
    begin
      Font.Height := newHeight;
      Assert(CalculatedWidthIsOK, 'The programmer added too many words to display correctly');
      Invalidate;
    end;
end;

function TWordMap.CalculatedWidthIsOK: Boolean;
var
  i, longestI, longestWidth, textWidth: Integer;
  sz: TSize;
  r: TRect;
begin
  Result := False;
  r := ClientRect;
  FMaxTextHeight := r.Height - FMinTopMargin shl 1;
  if FMaxTextHeight < MinAcceptableWordHeight then
    Exit;

  if Canvas.Font <> Font then
    Canvas.Font.Assign(Font);
  if Canvas.Font.Style <> [] then
    Canvas.Font.Style := [];

  for i := 0 to High(FWords) do
    begin
      sz := Canvas.TextExtent(FWords[i]);
      FNormalWidths[i] := sz.cx;
    end;

  longestWidth := 0;
  Canvas.Font.Style := [fsBold];
  for i := 0 to High(FWords) do
    begin
      sz := Canvas.TextExtent(FWords[i]);
      FHighWidths[i] := sz.cx;
      if longestWidth < sz.cx then
        begin
          longestWidth := sz.cx;
          longestI := i;
        end;
      if FNormalWordHeight < sz.cy then
        FNormalWordHeight := sz.cy;
    end;
  Canvas.Font.Style := [];

  textWidth := 0;
  for i := 0 to High(FNormalWidths) do
    case (i = {%H-}longestI) of
      True:  Inc(textWidth, FHighWidths[i]);
      False: Inc(textWidth, FNormalWidths[i]);
    end;

  FTotalPaintedWidth := textWidth + FWordSpacing*Succ(Length(FWords));
  Result := FTotalPaintedWidth <= r.Width;
  case Result of
    True:  Invalidate;
    False: AdjustFontToFitText;
  end;
end;

procedure TWordMap.DoSetBounds(ALeft, ATop, AWidth, AHeight: integer);
begin
  inherited DoSetBounds(ALeft, ATop, AWidth, AHeight);
  FMaxTextHeight := AHeight - FMinTopMargin shl 1;
  AdjustFontToFitText;
end;

class function TWordMap.GetControlClassDefaultSize: TSize;
begin
  Result.cx := 834;
  Result.cy := 28;
end;

function TWordMap.GetHighItemIndex: Integer;
begin
  Exit(High(FWords));
end;

function TWordMap.NeedToAdjustFontHeightToFit(out FontHeight: Integer): Boolean;
var
  maxTextW, curFontHeight, minFontHeight: Integer;
  drawFlags: LongWord = DT_CALCRECT or DT_NOPREFIX or DT_EXPANDTABS or DT_SINGLELINE;
  r: TRect;
  dc: HDC;
  testFont: TFont;
  oldFont: HGDIOBJ;
  widthOK, heightOK: Boolean;
begin
  Result := False;
  FontHeight := 0;
  if Length(FConcat) = 0 then
    Exit;
  maxTextW := Width - (FWordSpacing * Succ(Length(FWords)));
  minFontHeight := 6;
  testFont := TFont.Create;
  try
    testFont.Assign(Canvas.Font);
    curFontHeight := GetFontData(testFont.Reference.Handle).Height;
    r.Left := 0;
    r.Top := 0;
    dc := GetDC(Parent.Handle);
    try
      repeat
        oldFont := SelectObject(dc, HGDIOBJ(testFont.Reference.Handle));
        r.Right := maxTextW;
        r.Bottom := FMaxTextHeight;
        DrawText(dc, PChar(FConcat), Length(FConcat), r, drawFlags);
        widthOK := (r.Width <= maxTextW) and (r.Width > 0);
        heightOK := (r.Height <= FMaxTextHeight) and (r.Height > 0);
        SelectObject(dc, oldFont);
        if widthOK and heightOK then
          begin
            if (not Result) or (FontHeight < TestFont.Height) then
              FontHeight := TestFont.Height;
            Result := True;
            Break;
          end
        else
          begin
            Dec(curFontHeight);
            if curFontHeight >= minFontHeight then
              testFont.Height := curFontHeight;
          end;
      until curFontHeight < minFontHeight;
    finally
      ReleaseDC(Parent.Handle, dc);
    end;
  finally
    testFont.Free;
  end;
end;

procedure TWordMap.Paint;
var
  i, x, yLine, yNorm, xLeft: Integer;
  r: TRect;

  function SumLengths: Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i := 0 to High(FNormalWidths) do
      Inc(Result, FNormalWidths[i]);
    case ItemIndex of
      -1: ;
      else
        Inc(Result, FHighWidths[ItemIndex] - FNormalWidths[ItemIndex]);
    end;
  end;

  function SumLengthsTo(anIndex: Integer): Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i := 0 to anIndex do
      Inc(Result, FNormalWidths[i] + FWordSpacing);
  end;

begin
  if Length(FConcat) = 0 then
    Exit;

  r := ClientRect;
  if (r.Width <= FTotalPaintedWidth) then
    begin
      AdjustFontToFitText;
      Exit;
    end;

  xLeft := ((r.Width - FTotalPaintedWidth) shr 1) + FWordSpacing;
  yNorm := (r.Height - FNormalWordHeight) shr 1;
  yLine := yNorm + (FNormalWordHeight shr 1);
  Canvas.Brush.Color := Color; //clBackground;
  Canvas.Brush.Style := bsSolid;
  Canvas.FillRect(r);

  Canvas.Font.Assign(Font);
  Canvas.Brush.Style := bsClear;
  Canvas.Font.Color := FNormalColor;
  x := xLeft;
  for i := 0 to High(FWords) do
    begin
      case i = ItemIndex of
        True: begin
                Canvas.Pen.Color := FHighlightColor;
                Canvas.Font.Style := [fsBold];
                Canvas.Font.Color := FHighlightColor;
                Canvas.TextOut(x, Pred(yNorm), FWords[i]);
                Canvas.Font.Height := Font.Height;
                Canvas.Font.Color := clDkGray;
                Canvas.Font.Style := [];
                Inc(x, FHighWidths[i]);
                case (i = HighItemIndex) of
                  True: ;
                  False: Canvas.Line(x+2, yLine, x + FWordSpacing - 2, yLine);
                end;
                Inc(x, FWordSpacing);
              end;
        False: begin
                 Canvas.Pen.Color := clDkGray;
                 Canvas.Font.Color := clDkGray;
                 Canvas.TextOut(x, yNorm, FWords[i]);
                 Inc(x, FNormalWidths[i]);
                 case (i = HighItemIndex) of
                   True: ;
                   False: Canvas.Line(x+2, yLine, x + FWordSpacing - 2, yLine);
                 end;
                 Inc(x, FWordSpacing);
               end;
      end;
    end;
  for i := High(FWords) downto 0 do
    begin
      case ((i = ItemIndex) and (i > 0)) of
        True: begin
                Canvas.Pen.Color := FHighlightColor;
                x := xLeft + SumLengthsTo(Pred(i));
                Canvas.Line(x-2, yLine, x-FWordSpacing+2, yLine);
              end;
        False: ;
      end;
    end;
end;

procedure TWordMap.SetHighlightColor(aValue: TColor);
begin
  if aValue <> FHighlightColor then
    begin
      FHighlightColor := aValue;
      Invalidate;
    end;
end;

procedure TWordMap.SetItemIndex(aValue: Integer);
begin
  if (aValue <> FItemIndex) and (aValue < Length(FWords)) and (aValue > -2) then
    begin
      FItemIndex := aValue;
      Invalidate;
    end;
end;

procedure TWordMap.SetNormalColor(aValue: TColor);
begin
  if FNormalColor <> aValue then
    begin
      FNormalColor := aValue;
      Invalidate;
    end;
end;

procedure TWordMap.SetMinTopMargin(aValue: Integer);
begin
  if FMinTopMargin <> aValue then
    begin
      FMinTopMargin := aValue;
      Invalidate;
    end;
end;

procedure TWordMap.SetWordSpacing(aValue: Integer);
begin
  if FWordSpacing <> aValue then
    begin
      FWordSpacing := aValue;
      Invalidate;
    end;
end;
{%endregion TWordMap}

end.

