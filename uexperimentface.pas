unit uExperimentFace;

{
  Working Memory Analyser (Wizard Version 1.0.3)

  An open source Psychology software to investigate both individual
  differences in working memory capacity and the effect of working
  memory load on visual distractibility

  Copyright © 2020-2021 by Phil Duke and Giorgio Fuggetta

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
}

{
Special Thanks to:

 - skalogryz (email: unspecified), additional developed, optimised and compiled the software for MacOS.
 - Howard Page-Clark (email: pageclarkhoward@gmail.com), refactored earlier code to make the experiment
   resource-based rather than file-based, and developed the wizard interface and data-processing modules.
}

{$mode ObjFPC}{$H+}
{$ModeSwitch advancedrecords}

interface

uses
  {Classes, SysUtils,}
  SDL2, GL, glu;

const
  LeftEye = False;
  NearClipQuantity = 0.01;

type

  RFace = record
  private
    cosAngleOfRegard: Single;
    sinAngleOfRegard: Single;
    headX: Single;
    headY: Single;
    headZ: Single;
    halfIOD: Single;
    currentEye: Boolean;
    hasQuadbuffer: Boolean;
    glContext: TSDL_GLContext;
    joystick, joystick1, joystick2: PSDL_Joystick;
    procedure InitGL;
    function GetHasQuadBuffer: Boolean;
  public
    SDLWindow: PSDL_Window;
    procedure Init;
    procedure Render;
    procedure ProjectionTrans;
    function SetupSDL: Integer;
    procedure UnsetSDL;
    procedure MaximiseWindow;
  end;

implementation

uses
  LCLIntf, LCLType,
  uParams, uRStrings;

procedure RFace.InitGL;
begin
  glClearColor(0.0, 0.0, 0.0, 0.0);                        // Clear Colour buffer
  glClearDepth(1000.0);                                    // Depth buffer setup
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glEnable(GL_LINE_SMOOTH);                                // Enable GL antialiasing
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);                                 // Enable Depth Testing
  glDepthFunc(GL_LESS);                                    // The Type Of Depth Test To Do
  glMatrixMode(GL_MODELVIEW); glLoadIdentity;              // Clear the modelview and projection matrices
  glMatrixMode(GL_PROJECTION); glLoadIdentity;
end;

function RFace.GetHasQuadBuffer: Boolean;
var
  present: PInt;
begin
  if not Assigned(SDLWindow) then
    Exit(False);
  New(present);
  present^ := -1;
  SDL_GL_SetAttribute(SDL_GL_STEREO, 1);
  SDL_GL_GetAttribute(SDL_GL_STEREO, present); // present^==0 means the video card can do quad buffering
  Result := present^ = 0;
  Dispose(present);
end;

procedure RFace.Init;
begin
  cosAngleOfRegard := 1;
  sinAngleOfRegard := 0;
  halfIOD := 3.125;
  headX := 0; headY := 0; headZ := 0;
  currentEye := LeftEye;
  hasQuadBuffer := False;
end;

procedure RFace.Render;
begin
  SDL_GL_SwapWindow(SDLWindow);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
end;

procedure RFace.ProjectionTrans;
var
  eyePosX, eyePosZ: Single;
begin
  case hasQuadbuffer of
    True: begin
            case currentEye of
              LeftEye: glDrawBuffer(GL_BACK_LEFT);
              else
                glDrawBuffer(GL_BACK_RIGHT);
            end;
            glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
          end;
    False: ;
  end;
  case currentEye of
    LeftEye: begin
               eyePosX := -halfIOD * cosAngleOfRegard;
               eyePosZ :=  halfIOD * sinAngleOfRegard;
             end;
    else
      eyePosX :=  halfIOD * cosAngleOfRegard;
      eyePosZ := -halfIOD * sinAngleOfRegard;
  end;
  glMatrixMode(GL_PROJECTION);  glLoadIdentity;
  glFrustum( nearClipQuantity * (-ud.HalfWidthCM - eyePosX - headX),
             nearClipQuantity * ( ud.HalfWidthCM - eyePosX - headX),
             nearClipQuantity * (-ud.HalfHeightCM - headY),
             nearClipQuantity * ( ud.HalfHeightCM - headY),
             nearClipQuantity * (Distance + eyePosZ + headZ), 500000);
  gluLookAt(eyePosX + headX, headY, eyePosZ + headZ, eyePosX + headX, headY, -Distance, 0, 1, 0);
end;

function RFace.SetupSDL: Integer;
begin
  if (SDL_Init(SDL_INIT_AUDIO) < 0) then
    Exit(-1);
  if SDL_Init(SDL_INIT_VIDEO or SDL_INIT_JOYSTICK) < 0 then
    Exit(-3);
  SDLWindow := SDL_CreateWindow(PChar(rsWindow), 0, 0, ud.monWidth, ud.monHeight, SDL_WINDOW_FULLSCREEN or SDL_WINDOW_OPENGL);
  if not Assigned(SDLWindow) then
    Exit(-4);
  hasQuadbuffer := GetHasQuadBuffer;
  glContext := Nil;
  glContext := SDL_GL_CreateContext(SDLWindow);
  if not Assigned(glContext) then
    Exit(-5);
  SDL_GL_SetSwapInterval(1);
  SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );
  SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8 );
  SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
  SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
  SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
  InitGL;

  if SDL_NumJoysticks > 0 then
  begin
    SDL_JoystickEventState(SDL_ENABLE);
    joystick  := SDL_JoystickOpen(0);
    joystick1 := SDL_JoystickOpen(1);
    joystick2 := SDL_JoystickOpen(2);
  end;

  SDL_ShowCursor(SDL_DISABLE);
  Result := 0;
end;

procedure RFace.UnsetSDL;
begin
  SDL_ShowCursor(SDL_ENABLE);
  SDL_Quit;
end;

procedure RFace.MaximiseWindow;
begin
  SDL_MaximizeWindow(SDLWindow);
  SDL_ShowWindow(SDLWindow);
end;

end.

