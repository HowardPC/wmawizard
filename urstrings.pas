unit uRStrings;

{
  Working Memory Analyser (Wizard Version 1.0.3)

  An open source Psychology software to investigate both individual
  differences in working memory capacity and the effect of working
  memory load on visual distractibility

  Copyright © 2020-2021 by Phil Duke and Giorgio Fuggetta

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
}

{
Special Thanks to:

 - skalogryz (email: unspecified), additional developed, optimised and compiled the software for MacOS.
 - Howard Page-Clark (email: pageclarkhoward@gmail.com), refactored earlier code to make the experiment
   resource-based rather than file-based, and developed the wizard interface and data-processing modules.
}

{$mode objfpc}{$H+}

interface

resourcestring

  rs1To12 = '01'#10'02'#10'03'#10'04'#10'05'#10'06'#10'07'#10'08'#10'09'#10'10'#10'11'#10'12';
  rs1To31 = '01'#10'02'#10'03'#10'04'#10'05'#10'06'#10'07'#10'08'#10'09'#10'10'#10'11'#10'12'#10'13'#10'14'#10'15'#10'16'#10'17'#10'18'#10'19'#10'20'#10'21'#10'22'#10'23'#10'24'#10'25'#10'26'#10'27'#10'28'#10'29'#10'30'#10'31';
  rsAbout = 'About';
  rsAge = '  Age  ';
  rsAgeColon = 'Age: ';
  rsARIAL = 'ARIAL';
  rsAtoZ = 'A'#10'B'#10'C'#10'D'#10'E'#10'F'#10'G'#10'H'#10'I'#10'J'#10'K'#10'L'#10'M'#10'N'#10'O'#10'P'#10'Q'#10'R'#10'S'#10'T'#10'U'#10'V'#10'W'#10'X'#10'Y'#10'Z';
  rsChooseExperiment = 'Choose experiment';
  rsChooseExperimentLong = '  Choose which experiment to do (selection shows description)  ';
  rsCompleteQuestionnaire = 'Complete the questionnaire online';
  rsConsentShort = 'Consent';
  rsContinue = 'Continue';
  rsCorrect = 'Correct';
  rsCorrectColor = 'Correct_feedback_colour:'#9;
  rsCreateID = 'Create a ''Participant ID'' using the radio buttons. You will need this if you wish to withdraw later.';
  rsDayOfBirth = '  The Day of your birth (for example, ''17'')';
  rsDetailsForParticipant = 'Details for participant "%s"';
  rsDisplay = 'Screen:  ';
  rsDoYouReallyW = 'Do you really want to abandon this experiment?';
  rsExperiment = 'Experiment';
  rsExperiment01 = 'Exp_01_WM_capacity';
  rsExperiment02 = 'Exp_02_WM_load_and_distractor_processing';
  rsExperimentDetails = '  Experiment details - change only at your researcher''s request  ';
  rsExperimentItems = 'Experiment 1 Working Memory Capacity'#10'Experiment 2 Working Memory Load and Distractor processing';
  rsExperimentTab = 'Experiment:'#9;
  rsExperimentTerminated = 'Experiment completed successfully!'#10#10'Data folder is %s'#10#10'Data file is %s'#10;
  rsExperimentTraining = 'Experiment training phase';
  rsFathersInitial = '  Your father''s first initial (for example, Adam is ''A'')';
  rsFebDoesNotHave = 'February does not have 30 days';
  rsFemaleMale = 'Female'#10'Male';
  rsFileWriteError = 'File write error';
  rsFinalFeedback = 'Final feedback';
  rsFinish = 'Finish';
  rsFinishEarly = 'Finish early?';
  rsFixationColor = 'Fixation_colour:'#9;
  rsGatheringInfo = 'Gathering basic information';
  rsGenerateID = 'Generate a unique Participant ID';
  rsHandedness = '  Handedness  ';
  rsIAmAware = 'I am aware that I can withdraw at any time by pressing “Esc” while the computer task is running.';
  rsIHaveReadAboveText = 'I have read the text above (or a person with parental responsibility has read the information for children under'#10'  the age of 16), and consent to participate, and understand what is required.';
  rsIncorrect = 'Incorrect';
  rsIncorrectColor = 'Incorrect_feedback_colour:'#9;
  rsInputFile = '  Input file number  ';
  rsInstructions = 'Instructions';
  rsInstructionsEven = 'Instructions_EVEN_participants:'#9'even.bmp';
  rsInstructionsOdd = 'Instructions_ODD_participants:'#9'odd.bmp';
  rsIntroducing = 'Introducing Working Memory Analyser';
  rsIntroduction = 'Introduction';
  rsIUnderstandICanWithdraw = 'I understand I can withdraw my data after the experiment has finished, up to 4 months from my participation by emailing'#10'  the researcher with my unique Participant ID code.';
  rsLeftHanded = 'Left-handed'#10'Right-handed';
  rsManyThanks = 'Many thanks for participating';
  rsMonthOfBirth = '  The Month of your birth (for example, ''11'')';
  rsMothersInitial = '  Your mother''s first initial (for example, Mary is ''M'')';
  rsNext = 'Next';
  rsNoSelection = 'no selection made';
  rsOpenResults = 'Open your results file';
  rsParticipantColon = 'ParticipantID: ';
  rsParticipantConsent = 'Participant Consent (you must agree to all conditions in order to continue)';
  rsPauseColor = 'Pause_background_circle_colour:'#9;
  rsPauseMainTab = 'N_trials_before_pause_main:'#9;
  rsPauseTrainingTab = 'N_trials_before_pause_training:'#9;
  rsPlaceholdersColor = 'Placeholders_colour:'#9;
  rsProvideInfo = 'Provide information';
  rsQuestionnaireURL = 'https://roehamptonpsych.az1.qualtrics.com/jfe/form/SV_9zU7Nf3Ped7Ep8h';
  rsQuit = 'Quit';
  rsRandom = 'Random input file number';
  rsRunColor = 'Run_background_circle_colour:'#9;
  rsSDoesNotHave = '%s does not have 31 days';
  rsSession = '  Session number  ';
  rsSex = '  Sex  ';
  rsSexColon = 'Sex: ';
  rsShapesColor = 'Shapes_colour_%d:'#9;
  rsShowProgress = 'Show participant''s progress';
  rsSpecific = 'Specific input file number';
  rsStartExperiment = 'Start experiment';
  rsTrainingTria = 'Training trial results';
  rsUnableToCreate = 'Unable to create output file';
  rsUnableToDete = 'Unable to determine monitor characteristics - program will end';
  rsUploadData = 'Upload your output data (*.txt) file online';
  rsUploadURL = 'https://www.cognitoforms.com/GiorgioFuggetta/WorkingMemoryAnalyserUploadOutputFile2';
  rsUserDidNotFinish = 'User %s did not finish the experiment';
  rsWindow = 'Experiment window';
  rsYoungQuestionnaireURL = 'https://roehamptonpsych.az1.qualtrics.com/jfe/form/SV_3xfT225pO8j17rD';
  rsYourBadAccuracy = '  Your accuracy is %1.0f%%. You need to reach %1.0f%%. Please continue training.  ';
  rsYourGoodAccuracy = '  Your accuracy is %1.0f%%. Well done. Please continue with the main experiment.  ';
  rsYourID = 'Your ID';
  rsYourInitial = '  Your first initial (for example, Laura is ''L'')';

  rsIntroText =
    'The following pages prepare you for choosing and then doing an experiment '+
    'to investigate your "working memory". You need to provide a few details '+
    'about yourself, and give your consent to the researchers using your results.'+
    #10#10'Any saved data is anonymised, linked only to a unique ID '+
    'you will be asked to produce. This cannot be traced back to you, but enables the '+
    'researchers to link your work today with any experiment you might do in '+
    'the future, without us needing to ask your name.'+
    #10#10'The only personal information recorded is your age, your gender, and whether you are left-handed or right-handed.'+
    #10#10'After choosing which experiment to do, you need to practice the experiment '+
    'a few times to get familiar with watching little patterns (or letters) on the screen, '+
    'and reacting accordingly. The pattern(s) or letter(s) are shown twice, and you use your memory to recall what you have seen. '+
    'The program scores how accurately you remember what you have seen.'+
    #10#10'Before the experiment you are shown an instruction screen telling you which '+
    'mouse (or keypad) button to press when the second pattern shown matches the first pattern(s) shown, '+
    'and which button to press when the second pattern differs from all of the first ones.'+
    #10#10'When your score shows you have got the hang of what is needed, the practice phase ends '+
    'and you proceed to the actual experiment.'+
    #10#10'When you have finished you are given some feedback. The whole process takes an hour or more.';

  rsExp01WMCapacity = 'Experiment 1: Working Memory Capacity'+
    #10#10'This experiment assesses individual differences in working memory capacity using both abstract shapes'+
    'and letters to provide different types of working memory load.'+
    #10#10'You should keep a viewing distance of about 57 centimetres from the screen. If possible,'+
    ' you should use a mouse to respond. Put the mouse in line with the centre of your screen'+
    ' and your body. Your left index finger should be on the left mouse button. Put your right hand index'+
    ' finger on the right mouse button. In the absence of a mouse, please use the laptop’s touchpad.'+
    #10#10'This is a lengthy computer task, in which you are asked to do 16 training trials '+
    'before you start 8 blocks of 32 recorded trials. When you finish you will have completed 256 trials. '+
    'There will be a break between each block of trials because of the length of the experiment. '+
    'Fatigue could affect the results.'+
    #10#10'Please note that this is a Reaction Times experiment in which you will be timed, '+
    'so please try to be as quick and as accurate as possible when you respond.'+
    #10#10'Please fixate the central fixation dot placed at the centre of the screen while performing '+
    'these computer tasks. If you need to blink, you can do it after the feedback.'+
    #10#10'Thank you.';

  rsExp02WMLoadAndDistractorProcessing = 'Experiment 2: Working Memory Load and Distractor processing'+
    #10#10'This experiment assesses the effect of loading information in working memory on distractibility '+
    'of task-irrelevant visual information using both abstract shapes and letters to provide different types '+
    'of working memory load.'+
    #10#10'You should keep a viewing distance of about 57 centimetres from the screen. If possible, '+
    'you should use a mouse to respond. Put the mouse in line with the centre of your screen and your body. '+
    'Your left index finger should be on the left mouse button. Put your right hand index finger on the '+
    'right mouse button. In the absence of a mouse, please use the laptop’s touchpad.'+
    #10#10'This is a lengthy computer task in which you are asked to do 16 training trials '+
    'before you start 8 blocks of 32 recorded trials. When you finish you will have completed 256 trials. '+
    'There will be a break between each blocks of trials because of the length of the experiment. '+
    'Fatigue could affect the results.'+
    #10#10'Please note that this is a Reaction Times experiment in which you will be timed, '+
    'so please try to be as quick and as accurate as possible when you respond.'+
    #10#10'Please fixate the central fixation dot placed at the centre of the screen while performing '+
    'these computer tasks. If you need to blink, you can do it after the feedback.'+
    #10#10'Thank you.';

 rsHeaderLine = 'Experiment'#9'Date'#9'Start_Time'#9'Trial_Order_File_No'#9'ParticipantID'#9'Age'#9+
    'sex'#9'Handedness'#9'Display_Type'#9'Observer_number'#9'session_no'#9's1_marker'#9's2_marker'#9+
    's3_marker'#9's4_marker'#9's1_shape'#9's1_quad'#9's1_duration'#9's1_s2_isi'#9's2_shape_position_1(NW)'#9+
    's2_shape_position_2(NE)'#9's2_shape_position_3(SE)'#9's2_shape_position_4(SW)'#9's2_shape_position_5(centre)'#9+
    's2_duration'#9's2_s3_isi'#9's3_shape'#9's3_distractor_shape'#9's3_quad'#9's3_duration'#9's3_s4_isi'#9+
    's4_shape'#9's4_distractor_shape'#9's4_quad'#9's4_duration'#9'Response_Time_after_S4'#9'Feedback_shape'#9+
    'Feedback_duration_after_response_time'#9'ITI_after_feedback'#9's1_colour'#9's2_colour_position_1(NW)'#9+
    's2_colour_position_NE)'#9's2_colour_position_SE)'#9's2_colour_position_4(SW)'#9's2_colour_position_5(centre)'#9+
    's3_colour'#9's3_distractor_colour'#9's4_colour'#9'S4_distractor_colour'#9'keyMapping'#9'taskType'#9+
    'TMS_s3_SOA'#9'Experimental_Condition'#9'response'#9'Accuracy'#9'RT_ms'#9'RT_ms_minus_constant_error'#9+
    's1_onsetTime_ms'#9's2_onsetTime_ms'#9's3_onsetTime_ms'#9'TMS_onsetTime_ms'#9's4_onsetTime_ms'#9+
    'response_onsetTime_ms'#9'feedback_onsetTime_ms'#9'blank_onsetTime_ms'#9'user_paused_the_trial'#9+
    'Factor_1'#9'Factor_2'#9'Factor_3'#9'Factor_4'#9'Factor_5'#9'Factor_6'#9'Factor_7'#9'Factor_8'#9'Factor_9';

 rsAboutInfo = 'Working Memory Analyser (WizardVersion 1.0.3)'#10#10+
    'Copyright © 2020-2021 by Phil Duke and Giorgio Fuggetta'#10#10+
    'Contributors:'#10#10+
    'Dr Phil Duke (email: pad11@leicester.ac.uk) conceived and primarily developed the software for Windows.'#10#10+
    'Dr Giorgio Fuggetta (email: giorgio.fuggetta@roehampton.ac.uk) conceived the software and its modules Merge Data and Automatic Data Processing and created the sequences of cognitive tasks available as part of the software''s experiment library.'#10#10+
    'skalogryz (email: unspecified), additional developed, optimised and compiled the software for MacOS.'#10#10+
    'Howard Page-Clark (email: pageclarkhoward@gmail.com), refactored earlier code to make the experiment resource-based rather than file-based, and developed the wizard interface and data-processing modules.'#10#10+
    'The latest version of Working Memory Analyser and updated experiment library can be downloaded from: https://github.com/gfuggetta-lab/WorkingMemoryAnalyser.'#10#10+
    'This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License v3.0.'#10#10+
    'This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.';

 rsConsent = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">'#10+
'<html>'#10+
'<head>'#10+
'<meta http-equiv="content-type" content="text/html; charset=utf-8"/>'#10+
'<title></title>'#10+
'<style type="text/css">'#10+
'@page { size: 21.59cm 27.94cm; margin: 2.54cm }'#10+
'p { margin-bottom: 0.25cm; direction: ltr; color: #000000; line-height: 0.49cm; text-align: left; orphans: 2; widows: 2; background: transparent }'#10+
'p.western { font-family: "Calibri", sans-serif; font-size: 11pt; so-language: en-US }'#10+
'p.cjk { font-family: "Times New Roman"; font-size: 11pt; so-language: ar-SA }'#10+
'p.ctl { font-family: "Liberation Serif"; font-size: 11pt }'#10+
'a:link { color: #0000ff; text-decoration: underline }'#10+
'a:visited { color: #800000; so-language: zxx; text-decoration: underline }'#10+
'</style>'#10+
'</head>'#10+
'<body lang="en-GB" text="#000000" link="#0000ff" vlink="#800000" dir="ltr"><p lang="en-US" class="western" style="margin-bottom: 0.28cm; line-height: 0.41cm">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><font size="4" style="font-size: 14pt"><span lang="en-GB"><b>PARTICIPANT'#10+
'CONSENT FORM</b></span></font></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.28cm; line-height: 0.41cm">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">Empirical'#10+
'data collection of research project to assess individual differences'#10+
'in working memory capacity and visual distractibility with working'#10+
'memory load.</span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.28cm; line-height: 0.41cm">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB"><b>Brief'#10+
'description of research project</b></span></font></font><font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">:</span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.28cm; line-height: 0.41cm">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">This'#10+
'project is being conducted both in the laboratory and remotely online'#10+
'to collect a wide range of participants. A typical experiment will'#10+
'take around 60 minutes to complete and will require the completion of'#10+
'a cognitive task and a few questionnaires to explore individual'#10+
'variation in the effects. The cognitive task will be performed using'#10+
'Working Memory Analyser, an open source free Psychology program which'#10+
'can examine both individual differences in working memory capacity'#10+
'and the effect of working memory load on visual distractibility. Your'#10+
'voluntary participation will provide valuable insight into how our'#10+
'cognitive system works. This in turn can inform cognitive theories of'#10+
'working memory and attention.</span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.28cm; line-height: 0.41cm">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">For'#10+
'this study, information is initially saved to a secure online server'#10+
'hosted by Cognito forms and Qualtrics. These are trusted and secure'#10+
'sites which are widely used and can only be accessed by the research'#10+
'team who have passcodes for these secure servers. For more'#10+
'information see '#10+
'</span></font></font><font color="#954f72"><font face="Arial, sans-serif"><span lang="en-GB"><u><a href="https://www.cognitoforms.com/support/74/entries/data-security">https://www.cognitoforms.com/support/74/entries/data-security</a>'#10+
'</u></span></font></font><font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">and'#10+
'</span></font></font><a href="https://www.qualtrics.com/security-statement/"><font color="#954f72"><font face="Arial, sans-serif"><span lang="en-GB"><u>https://www.qualtrics.com/security-statement/</u></span></font></font></a><font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">.</span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.28cm; line-height: 0.41cm">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">Personal'#10+
'data is kept completely anonymised. To enable this you are asked to'#10+
'create a 7-digit “Participant ID code” which is associated with'#10+
'your data. You will need this code if you later wish to withdraw your'#10+
'data after participating. You may withdraw your data up to 4 months'#10+
'after your completion of the study. Withdrawal can be completed by'#10+
'contacting the lead researcher of the study by email with your unique'#10+
'participant ID code. You do this by emailing '#10+
'&nbsp;</span></font></font><a href="mailto:giorgio.fuggetta@roehampton.ac.uk"><font color="#954f72"><font face="Arial, sans-serif"><span lang="en-GB"><u>giorgio.fuggetta@roehampton.ac.uk</u></span></font></font></a><font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">,'#10+
'and you do not need to provide a reason for withdrawal.</span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.28cm; line-height: 0.41cm">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">If'#10+
'you withdraw your data, then all data linked to your submission will'#10+
'be permanently deleted. After the 4-month period, your data will be'#10+
'used for research, but will always be stored in an anonymous format'#10+
'that cannot be traced back to you.  The data will be retained for a'#10+
'minimum of 10 years in an anonymised form. If these projects are'#10+
'carried out in partnership with other organisations or academic'#10+
'institutions, your anonymised data will be shared with them.</span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.28cm; line-height: 0.41cm">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">The'#10+
'data will be written up as academic articles for publication in'#10+
'peer-reviewed journals and presentation at academic conferences.'#10+
'Additionally, the anonymised data will be stored online on the Open'#10+
'Science Framework (</span></font></font><a href="https://osf.io/"><font color="#954f72"><font face="Arial, sans-serif"><span lang="en-GB"><u>https://osf.io/</u></span></font></font></a><font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">)'#10+
'which facilitates sharing of scientific findings with other'#10+
'academics.</span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.64cm; line-height: 106%">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB"><b>People'#10+
'aged 16 or over are entitled to consent to their own data collection.'#10+
'This can only be overruled in exceptional circumstances. </b></span></font></font><font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">Like'#10+
'adults, young people (aged 16 or 17) are presumed to have sufficient'#10+
'capacity to decide about their own data collection, unless there is'#10+
'significant evidence to suggest otherwise.</span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.64cm; line-height: 106%">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB"><b>Children'#10+
'under the age of 16 must have a person with&nbsp;parental'#10+
'responsibility to give consent for them. This is even if the children'#10+
'are believed to&nbsp;have enough intelligence, competence and'#10+
'understanding to fully appreciate what is involved in their data'#10+
'collection.</b></span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.64cm; line-height: 106%">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">This'#10+
'could be:</span></font></font></p>'#10+
'<ul>'#10+
'<li><p lang="en-GB" class="western" style="margin-bottom: 0.21cm; line-height: 106%">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">the'#10+
'child''s mother or father</span></font></font></p>'#10+
'<li><p lang="en-GB" class="western" style="margin-bottom: 0.21cm; line-height: 106%">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">the'#10+
'child''s legally appointed guardian</span></font></font></p>'#10+
'<li><p lang="en-GB" class="western" style="margin-bottom: 0.21cm; line-height: 106%">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">a'#10+
'person with a residence order concerning the child</span></font></font></p>'#10+
'<li><p lang="en-GB" class="western" style="margin-bottom: 0.21cm; line-height: 106%">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">a'#10+
'local authority designated to care for the child</span></font></font></p>'#10+
'<li><p lang="en-GB" class="western" style="margin-bottom: 0cm; line-height: 106%">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">a'#10+
'local authority or person with an emergency protection order for the'#10+
'child</span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0cm; line-height: 106%">'#10+
'</p>'#10+
'</ul>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.64cm; line-height: 106%">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">A'#10+
'person with parental responsibility must have the capacity to give'#10+
'consent.</span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.28cm; line-height: 0.41cm">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">If'#10+
'you wish to have a copy of any information from this consent page,'#10+
'please contact the lead researcher for a pdf copy'#10+
'(</span></font></font><a href="mailto:giorgio.fuggetta@roehampton.ac.uk"><font color="#954f72"><font face="Arial, sans-serif"><span lang="en-GB"><u>giorgio.fuggetta@roehampton.ac.uk</u></span></font></font></a><font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">).</span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.28cm; line-height: 0.41cm">'#10+
'<br/>'#10+
'<br/>'#10#10+
'</p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.28cm; line-height: 0.41cm">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB"><b>Investigator'#10+
'contact details:</b></span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0cm; line-height: 106%">'#10+
'<font color="#3b3838"><font face="Arial, sans-serif"><span lang="en-GB">Dr'#10+
'Giorgio Fuggetta</span></font></font><br/>'#10+
'<font color="#3b3838"><font face="Arial, sans-serif"><span lang="en-GB">Senior'#10+
'Lecturer in Psychology</span></font></font><br/>'#10+
'<font color="#3b3838"><font face="Arial, sans-serif"><span lang="en-GB">Department'#10+
'of Psychology</span></font></font><br/>'#10+
'<font color="#3b3838"><font face="Arial, sans-serif"><span lang="en-GB">University'#10+
'of Roehampton | London | SW15 4JD</span></font></font><br/>'#10+
'<font color="#3b3838"><font face="Arial, sans-serif"><span lang="en-GB">Parkstead'#10+
'House</span></font></font><br/>'#10+
'<font color="#3b3838"><font face="Arial, sans-serif"><span lang="en-GB">Email:'#10+
'</span></font></font><a href="mailto:giorgio.fuggetta@roehampton.ac.uk"><font color="#954f72"><font face="Arial, sans-serif"><span lang="en-GB"><u>giorgio.fuggetta@roehampton.ac.uk</u></span></font></font></a></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.28cm; line-height: 0.41cm">'#10+
'<br/>'#10+
'<br/>'#10#10+
'</p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.28cm; line-height: 0.41cm">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB"><b>Consent'#10+
'statement:</b></span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.28cm; line-height: 0.41cm">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">I'#10+
'agree to take part in this research and am aware that I am free to'#10+
'withdraw at any point without giving a reason by contacting Dr'#10+
'Giorgio Fuggetta (</span></font></font><a href="mailto:giorgio.fuggetta@roehampton.ac.uk"><font color="#954f72"><font face="Arial, sans-serif"><span lang="en-GB"><u>giorgio.fuggetta@roehampton.ac.uk</u></span></font></font></a><font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">)'#10+
'with my Participant ID code. I understand that if I do withdraw, my'#10+
'data may not be erased automatically but will only be used in an'#10+
'anonymised form as part of an aggregated dataset. I understand that'#10+
'the personal data collected from me during the course of these'#10+
'projects will be used for the purposes outlined above in the public'#10+
'interest.</span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.28cm; line-height: 0.41cm">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">By'#10+
'acknowledging your consent to this form, you are confirming that you'#10+
'have been informed about and understand the University’s </span></font></font><a href="https://www.roehampton.ac.uk/globalassets/documents/ethics/dec-2019/data-privacy-notice-for-research-partcipants.docx"><font color="#954f72"><font face="Arial, sans-serif"><span lang="en-GB"><u>Data'#10+
'Privacy Notice for Research Participants</u></span></font></font></a><font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">.</span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.28cm; line-height: 0.41cm">'#10+
'<font color="#000000"><font face="Arial, sans-serif"><span lang="en-GB">The'#10+
'information you have provided will be treated in confidence by the'#10+
'researcher and your identity will be protected in the publication of'#10+
'any findings. The purpose of the research may change over time, and'#10+
'your data may be re-used for research projects by the University in'#10+
'the future. If this is the case, you will normally be provided with'#10+
'additional information about any further new project.</span></font></font></p>'#10+
'<p lang="en-GB" class="western" style="margin-bottom: 0.28cm; line-height: 106%">'#10+
'<br/>'#10+
'<br/>'#10#10+
'</p>'#10+
'</body>'#10+
'</html>'#10;

implementation

end.

