unit uTrainingFeedback;

{
  Working Memory Analyser (Wizard Version 1.0.3)

  An open source Psychology software to investigate both individual
  differences in working memory capacity and the effect of working
  memory load on visual distractibility

  Copyright © 2020-2021 by Phil Duke and Giorgio Fuggetta

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
}

{
Special Thanks to:

 - skalogryz (email: unspecified), additional developed, optimised and compiled the software for MacOS.
 - Howard Page-Clark (email: pageclarkhoward@gmail.com), refactored earlier code to make the experiment
   resource-based rather than file-based, and developed the wizard interface and data-processing modules.
}

{$mode ObjFPC}{$H+}

interface

uses
  SysUtils,
  Forms, Graphics, StdCtrls, ExtCtrls, Controls,
  uParams, uRStrings;

type

  TFeedbackDlg = class(TForm)
  private
    Panel: TPanel;
    ContinueButton: TButton;
  public
    constructor Create(anAccuracy: Single); reintroduce;
  end;

  procedure ShowTrainingFeedbackModal(anAccuracy: Single);

implementation

uses
  uWizardAndPanels;

procedure ShowTrainingFeedbackModal(anAccuracy: Single);
var
  dlg: TFeedbackDlg;
begin
  dlg := TFeedbackDlg.Create(anAccuracy);
  try
    dlg.ShowModal;
  finally
    dlg.Free;
  end;
end;

{ TFeedbackDlg }

constructor TFeedbackDlg.Create(anAccuracy: Single);
begin
  inherited CreateNew(Nil);
  BorderStyle := bsDialog;
  Position := poMainFormCenter;
  Constraints.MinWidth := 800;
  Constraints.MinHeight := 400;
  Caption := rsTrainingTria;

  Panel := TPanel.Create(Self);
  with Panel do
    begin
      Align := alClient;
      Font.Color := clWindowText;
      Font.Height := -20;
      case (anAccuracy < MinimumTrainingAccuracy) of
        True:  Caption := Format(rsYourBadAccuracy,  [anAccuracy*100, MinimumTrainingAccuracy*100]);
        False: Caption := Format(rsYourGoodAccuracy, [anAccuracy*100]);
      end;
      Parent := Self;
    end;

  ContinueButton := TButton.Create(Self);
  with ContinueButton do
    begin
      Caption := rsContinue;
      AutoSize := True;
      ModalResult := mrClose;
      Anchors := [akRight, akBottom];
      AnchorSideRight.Control := Panel;
      AnchorSideRight.Side := asrRight;
      AnchorSideBottom.Control := Panel;
      AnchorSideBottom.Side := asrBottom;
      BorderSpacing.Around := DblMargin;
      Parent := Panel;
    end;
end;

end.

