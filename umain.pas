unit uMain;

{
  Working Memory Analyser (Wizard Version 1.0.3)

  An open source Psychology software to investigate both individual
  differences in working memory capacity and the effect of working
  memory load on visual distractibility

  Copyright © 2020-2021 by Phil Duke and Giorgio Fuggetta

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
}

{
Special Thanks to:

 - skalogryz (email: unspecified), additional developed, optimised and compiled the software for MacOS.
 - Howard Page-Clark (email: pageclarkhoward@gmail.com), refactored earlier code to make the experiment
   resource-based rather than file-based, and developed the wizard interface and data-processing modules.
}

{$mode objfpc}{$H+}

interface

uses
  Classes,
  Forms, Controls,
  uWizardAndPanels;

type
  TMainForm = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    mList: TList;
    function GetMonitorIndexForPointZero: Integer;
    procedure Quit(askForConfirmation: Boolean);
  public
    wiz: TPageWizard;
  end;

var
  MainForm: TMainForm;

implementation

uses
  process, SysUtils, Types,
  Dialogs, LCLIntf, FileUtil,
  edidMonitorUtils,
  uParams, uRStrings;

{$R *.lfm}

procedure TMainForm.FormCreate(Sender: TObject);
var
  i: Integer;
  p: Pointer;
  m: TMonitor absolute p;
begin
  mList := TList.Create;
  try
    case GetSysMonitors(mList) of
      False: begin
               ShowMessage(rsUnableToDete);
               Application.Terminate;
             end;
      True: case mList.Count of
              0: begin
                   ShowMessage(rsUnableToDete);
                   Application.Terminate;
                 end;
              otherwise
                i := GetMonitorIndexForPointZero;
                case i of
                  -1: begin
                        ShowMessage(rsUnableToDete);
                        Application.Terminate;
                      end;
                  otherwise
                    ud.SetMonitor(TMonitor(mList[i]));
                end;
            end;
    end;
  finally
    for p in mList do
      m.Free;
    mList.Free;
  end;
  wiz := TPageWizard.Create(Self);
  wiz.Align := alClient;
  wiz.OnQuit := @Quit;
  wiz.Parent := Self;

  wiz.AddNewSheet(rsIntroducing, rsIntroduction, pkIntro);
  wiz.AddNewSheet(rsParticipantConsent, rsConsentShort, pkConsent);
  wiz.AddNewSheet(rsGenerateID, rsYourID, pkUniqueID);
  wiz.AddNewSheet(rsGatheringInfo, rsProvideInfo, pkGetInfo);
  wiz.AddNewSheet(rsInstructions, rsInstructions, pkInstructions);
  wiz.AddNewSheet(rsExperimentTraining, rsExperiment, pkExperiment);
  wiz.AddNewSheet(rsFinalFeedback, rsFinish, pkFeedback);
end;

function TMainForm.GetMonitorIndexForPointZero: Integer;
var
  m: TMonitor;
begin
  for Result := 0 to mList.Count-1 do
    begin
      m := TMonitor(mList[Result]);
      if not Assigned(m) then
        Continue;
      if PtInRect(m.Bounds, ClientToScreen(Point(0, 0))) then
        Exit;
    end;
  Result := -1;
end;

procedure TMainForm.Quit(askForConfirmation: Boolean);
begin
  case askForConfirmation of
    True:  if MessageDlg(rsFinishEarly, rsDoYouReallyW, mtConfirmation, mbYesNo, 0, mbNo) = mrYes then
             MainForm.Close;
    False: MainForm.Close;
  end;
end;

end.

