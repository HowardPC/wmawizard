unit uTrials;

{
  Working Memory Analyser (Wizard Version 1.0.3)

  An open source Psychology software to investigate both individual
  differences in working memory capacity and the effect of working
  memory load on visual distractibility

  Copyright © 2020-2021 by Phil Duke and Giorgio Fuggetta

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
}

{
Special Thanks to:

 - skalogryz (email: unspecified), additional developed, optimised and compiled the software for MacOS.
 - Howard Page-Clark (email: pageclarkhoward@gmail.com), refactored earlier code to make the experiment
   resource-based rather than file-based, and developed the wizard interface and data-processing modules.
}

{$mode ObjFPC}{$H+}
{$ModeSwitch advancedrecords}

interface

uses
  Classes, SysUtils, Types,
  LCLIntf, LCLType, Controls, Forms, Dialogs,
  SDL2, {SDL2_Mixer,} SDL2_ttf, GL,
  uExperimentFace, uInpFiles, uRStrings, uParams;


  function RunExperiment: Integer;

var
  ef: RFace;

implementation

uses
  Math,
  LazFileUtils, Graphics,
  uMain;

function RunExperiment: Integer;
var
  c, state, trialIdx: Integer;
  eventTime: UInt32;

  cumFrameCount: Integer;
  nResponseFrames: Integer;
  rr: RResponse;
  rtAfterS4Over1000, rtMax: Single;
  timeOfExperimentStart: UInt32;
  TMSFrameIdx: Integer;

  backgroundRadiusCM: Single;
  cueRadiusCM: Single;
  fixSpotRadiusCM: Single;
  imageFeedbackCM: Single;
  imageSizeCM: Single;
  placeholderRadiusCM: Single;
  s2SampleRadiusCM: Single;
  shapeSizeCM: Single;
  targetRadiusCM: Single;
  x, y: Single;

  dlCircle, dlCircleOutline, dlCircleBackground, dlCircleEa, dlSquareEa, dlDiamond, dlHex, dlTriangle,
      dlBox, dlRing, dlCross, dlBarHoriz, dlBarVert, dlStar: GLuint;

  pfGeneral: PTTF_Font;
  pfFeedback: PTTF_Font;
  pfStimulus: PTTF_Font;
  correctfColor, incorrectfColor, backgroundfColor: TSDL_Color;

  hasResponded: Boolean;
  isSuspended: Boolean;
  isVeryFirstTrial: Boolean;
  targetMatchesCue: Boolean;

  ti: SaWords;
  {%H-}ts: SaStrings;
  trialArr: SaTrialIterations;
  //sounds: SaMixChunk; // n/a
  surfaceArr: TSurfaceArr;

  procedure InitialiseParameters;
  begin
    backgroundRadiusCM :=  Distance * Tan(HalfBackgroundDiamDeg * PiOver180);
    cueRadiusCM        :=  Distance * Tan(HalfPlaceholdersDiamDeg * PiOver180);
    fixSpotRadiusCM :=     Distance * Tan(FixationDotDeg * PiOver180) / 2;
    imageFeedbackCM :=     Distance * Tan(ImageFeedbackDeg * PiOver180);
    imageSizeCM :=         Distance * Tan(ImageSizeDeg * PiOver180);
    placeholderRadiusCM := Distance * Tan(PlaceholderRadiusDeg * PiOver180);
    s2SampleRadiusCM :=    Distance * Tan(S2SampleDiameterDeg * PiOver180) / 2;
    shapeSizeCM :=         Distance * Tan(ShapeSizeDeg * PiOver180);
    targetRadiusCM  :=     Distance * Tan(HalfPlaceholdersDiamDeg * PiOver180);
    correctfColor    := CorrectFeedbackColour.ToSDLColor(0);
    incorrectfColor  := IncorrectFeedbackColour.ToSDLColor(0);
    backgroundfColor := RunBackgroundCircleColour.ToSDLColor(0);
    GetTrialData(ud.exptNo, ud.inputFileNo, trialArr);
    rr.Init;
  end;

  procedure SaveBmp;
  var
    surface: PSDL_Surface;
  begin
    surface := SDL_CreateRGBSurface(0, ud.monWidth, ud.monHeight, 24, $000000ff,$0000ff00,$00ff0000,$ff000000);
    glReadBuffer(GL_FRONT);
    glReadPixels(0, 0, ud.monWidth, ud.monHeight, GL_RGB, GL_UNSIGNED_BYTE, surface^.pixels);
    SDL_SaveBMP(surface, 'Screenshot.bmp');
    SDL_FreeSurface(surface);
  end;

  procedure PollEvent;
  var
    event: TSDL_Event;
    keysym: PSDL_keysym;
  begin
    State := -1;
    eventTime := 0;
    while (SDL_PollEvent(@event) = 1) do
      begin
        case {%H-}event.type_ of
          SDL_JOYBUTTONDOWN:  begin
                                state := event.jbutton.button;
                                eventTime := event.jbutton.timestamp;
                              end;
          SDL_MOUSEBUTTONDOWN: begin
                                 state := event.button.button;
                                 eventTime := event.button.timestamp;
                               end;
          SDL_KEYDOWN: if (event.key._repeat = 0) then
                         begin
                           keysym := @event.key.keysym;
                           state := keysym^.sym;
                           eventTime := event.key.timestamp;
                           case keysym^.sym of
                             SDLK_P: isSuspended := not isSuspended;
                             SDLK_ESCAPE: raise ExperimentTerminateException.Create('User pressed escape');
                             SDLK_PRINTSCREEN: SaveBMP;
                           end;
                         end;
        end; //case
      end; //while
  end;

  procedure CheckKeysAndButtons;
  begin
    PollEvent;
    // keyboard 'a/f' or keypad 1/4, or mouse button left/right
    case state of
      SDLK_a, SDLK_KP_1, SDL_BUTTON_LEFT:  state := ResponseLeftButton;
      SDLK_f, SDLK_KP_4, SDL_BUTTON_RIGHT: state := ResponseRightButton;
      else
        state := -1;
        eventTime := 0;
    end;
  end;

  procedure Circle(XCoord, Ycoord, ZCoord, OuterRadius, LineWidth: Single;
                   Npoints: Integer; {%H-}Colour: array of Real; Filled: Boolean);
  var
    xOuter, yOuter, xInner, yInner: TSingleDynArray;
    c: Integer;
    innerRadius: Single;
  begin
    InnerRadius := OuterRadius - LineWidth;
    GetCircleCoords(Xouter, Youter, OuterRadius, Npoints);
    GetCircleCoords(Xinner, Yinner, InnerRadius, Npoints);
    glColor4f(Colour[0], Colour[1], Colour[2], Colour[3]);
    case Filled of
      True: begin
              glBegin(GL_TRIANGLE_STRIP);
                for c:= 0 to Npoints-1 do
                  begin
                    glVertex3f(XCoord+Xouter[c],YCoord+Youter[c],ZCoord);
                    glVertex3f(XCoord,YCoord,ZCoord);
                  end;
                glVertex3f(XCoord+Xouter[Npoints-1],YCoord+Youter[Npoints-1],ZCoord);
                glVertex3f(XCoord,YCoord,ZCoord);
                glVertex3f(XCoord+Xouter[0],YCoord+Youter[0],ZCoord);
              glEnd;
            end;
      False: begin
               glBegin(GL_TRIANGLE_STRIP);
                 for c:=0 to Npoints-1 do
                   begin
                     glVertex3f(XCoord+Xouter[c],YCoord+Youter[c],ZCoord);
                     glVertex3f(XCoord+Xinner[c],YCoord+Yinner[c],ZCoord);
                   end;
                 glVertex3f(XCoord+Xouter[0],YCoord+Youter[0],ZCoord);
                 glVertex3f(XCoord+Xinner[0],YCoord+Yinner[0],ZCoord);
               glEnd;
             end;
     end;
  end;

  procedure Circle(XCoord, Ycoord, ZCoord, OuterRadius, LineWidth: Single;
                   Npoints: Integer; Filled: Boolean);
  var
    xOuter, yOuter, xInner, yInner: TSingleDynArray;
    c: Integer;
    innerRadius: Single;
  begin
    innerRadius := OuterRadius - LineWidth;
    GetCircleCoords(Xouter, Youter, OuterRadius, Npoints);
    GetCircleCoords(Xinner, Yinner, InnerRadius, Npoints);
    case Filled of
      True: begin
              glBegin(GL_TRIANGLE_STRIP);
                for c:= 0 to Npoints-1 do
                  begin
                    glVertex3f(XCoord+Xouter[c],YCoord+Youter[c],ZCoord);
                    glVertex3f(XCoord,YCoord,ZCoord);
                  end;
                glVertex3f(XCoord+Xouter[Npoints-1],YCoord+Youter[Npoints-1],ZCoord);
                glVertex3f(XCoord,YCoord,ZCoord);
                glVertex3f(XCoord+Xouter[0],YCoord+Youter[0],ZCoord);
              glEnd;
            end;
      False: begin
               glBegin(GL_TRIANGLE_STRIP);
                 for c:=0 to Npoints-1 do
                   begin
                     glVertex3f(XCoord+Xouter[c],YCoord+Youter[c],ZCoord);
                     glVertex3f(XCoord+Xinner[c],YCoord+Yinner[c],ZCoord);
                   end;
                 glVertex3f(XCoord+Xouter[0],YCoord+Youter[0],ZCoord);
                 glVertex3f(XCoord+Xinner[0],YCoord+Yinner[0],ZCoord);
               glEnd;
             end;
     end;
  end;

  procedure DrawFixationSpot(spotSize: Single; fixColor: TColourReal);
  begin
    Circle(0, 0, -Distance, spotSize, 0, 10, [fixColor.r, fixColor.g, fixColor.b, 1], True);
  end;

  procedure DrawBackgroundFixation(contextColour: TColourReal);
  begin
    glMatrixMode(GL_MODELVIEW); glLoadIdentity;
    DrawFixationSpot(fixSpotRadiusCM, FixationColour);
    glColor3f(contextColour.r,contextColour.g,contextColour.b);
    glCallList(dlCircleBackground);
  end;

  procedure Bar(XCoord, Ycoord, ZCoord, Length, Width, Theta: Single);
  var
    p1, p2, p3, p4: Rxyz;
    w2, l2: Single;
  begin
    w2 := Width/2;
    l2 := Length/2;
    p1.Init(-l2,  w2, 0);
    p2.Init(-l2, -w2, 0);
    p3.Init( l2, -w2, 0);
    p4.Init( l2,  w2, 0);
    p1.Rotate(theta, 'z');
    p2.Rotate(theta, 'z');
    p3.Rotate(theta, 'z');
    p4.Rotate(theta, 'z');
    glBegin(gl_polygon);
        glVertex3f(Xcoord+p1.x, Ycoord+p1.y, Zcoord+p1.z);
        glVertex3f(Xcoord+p2.x, Ycoord+p2.y, Zcoord+p2.z);
        glVertex3f(Xcoord+p3.x, Ycoord+p3.y, Zcoord+p3.z);
        glVertex3f(Xcoord+p4.x, Ycoord+p4.y, Zcoord+p4.z);
    glEnd;
  end;

  procedure Pentagram(XCoord, Ycoord, ZCoord, OuterRadius: Single);   // coords are cm, Size is degrees
  var
    Xouter, Youter: TSingleDynArray;
  begin
    GetCircleCoords(Xouter, Youter, OuterRadius, 5);
    glBegin(GL_POLYGON);
      glVertex3f(XCoord,YCoord,ZCoord);
      glVertex3f(XCoord+Xouter[2],YCoord+Youter[2],ZCoord);
      glVertex3f(XCoord+Xouter[0],YCoord+Youter[0],ZCoord);
      glVertex3f(XCoord,YCoord,ZCoord);
    glEnd;
    glBegin(GL_POLYGON);
      glVertex3f(XCoord,YCoord,ZCoord);
      glVertex3f(XCoord+Xouter[0],YCoord+Youter[0],ZCoord);
      glVertex3f(XCoord+Xouter[3],YCoord+Youter[3],ZCoord);
      glVertex3f(XCoord,YCoord,ZCoord);
    glEnd;
    glBegin(GL_POLYGON);
      glVertex3f(XCoord,YCoord,ZCoord);
      glVertex3f(XCoord+Xouter[1],YCoord+Youter[1],ZCoord);
      glVertex3f(XCoord+Xouter[4],YCoord+Youter[4],ZCoord);
      glVertex3f(XCoord,YCoord,ZCoord);
    glEnd;
    glBegin(GL_POLYGON);
      glVertex3f(XCoord,YCoord,ZCoord);
      glVertex3f(XCoord+Xouter[4],YCoord+Youter[4],ZCoord);
      glVertex3f(XCoord+Xouter[2],YCoord+Youter[2],ZCoord);
      glVertex3f(XCoord,YCoord,ZCoord);
    glEnd;
    glBegin(GL_POLYGON);
      glVertex3f(XCoord,YCoord,ZCoord);
      glVertex3f(XCoord+Xouter[3],YCoord+Youter[3],ZCoord);
      glVertex3f(XCoord+Xouter[1],YCoord+Youter[1],ZCoord);
      glVertex3f(XCoord,YCoord,ZCoord);
    glEnd;
  end;

  procedure RenderText(pfont: PTTF_Font; const aText: String; x, y: Single; colr: TSDL_Color; scale: Single);
  var
    textureID: GLUint;
    fontSurface, destSurface: PSDL_Surface;
  begin
    fontSurface := TTF_RenderText_Solid(pfont, PChar(aText), colr);
    destSurface := SDL_CreateRGBSurface(0, fontSurface^.w, fontSurface^.h, 24, $000000ff,$0000ff00,$00ff0000,$ff000000);
    SDL_blitsurface(fontsurface, Nil, destSurface, Nil);
    glGenTextures(1, @textureID);            // create a new texture
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, fontsurface^.w, fontsurface^.h, 0, GL_RGB, GL_UNSIGNED_BYTE, destSurface^.pixels);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);   // Select how the texture image is combined with existing image
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); // Texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glBegin(GL_QUADS);
      glTexCoord2f(0,1); glVertex2f(x, y);
      glTexCoord2f(1,1); glVertex2f(x+fontSurface^.w*scale*(1), y);
      glTexCoord2f(1,0); glVertex2f(x+fontSurface^.w*scale*(1), y+fontSurface^.h*scale*(1));
      glTexCoord2f(0,0); glVertex2f(x, y+fontSurface^.h*scale*(1));
    glEnd();
    glDisable(GL_TEXTURE_2D);
    glDeleteTextures(1, @textureID);
    SDL_FreeSurface(fontSurface); // clean up
    SDL_FreeSurface(destSurface);
  end;

  procedure RenderTextWithBackColorCentred(pfont: PTTF_Font; const aText: String; fCol, bCol: TSDL_Color; x, y, scale: Single);
  var
    textureID: GLUint;
    fontSurface, destSurface: PSDL_Surface;
  begin
    if aText = '' then Exit;
    //writeln('RenderTextWithBackColorCentred ');
    fontSurface := TTF_RenderUTF8_Shaded(pfont, PChar(aText), fCol, bCol);   // create 24bit destination surface with little-endian byte order
    destSurface := SDL_CreateRGBSurface(0, fontSurface^.w, fontSurface^.h, 24, $000000ff,$0000ff00,$00ff0000,$ff000000);
    SDL_BlitSurface(fontsurface, Nil, destSurface, Nil);
    glGenTextures(1, @textureID); // create a new texture
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, fontsurface^.w, fontsurface^.h, 0, GL_RGB, GL_UNSIGNED_BYTE, destSurface^.pixels);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE); // Select how the texture image is combined with existing image
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);  // Texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glBegin(GL_QUADS);
      glTexCoord2f(0,1); glVertex2f(x-((fontSurface^.w/2)*scale), y-((fontSurface^.h/2)*scale));
      glTexCoord2f(1,1); glVertex2f(x+((fontSurface^.w/2)*scale), y-((fontSurface^.h/2)*scale));
      glTexCoord2f(1,0); glVertex2f(x+((fontSurface^.w/2)*scale), y+((fontSurface^.h/2)*scale));
      glTexCoord2f(0,0); glVertex2f(x-((fontSurface^.w/2)*scale), y+((fontSurface^.h/2)*scale));
    glEnd();
    glDisable(GL_TEXTURE_2D);
    glDeleteTextures(1, @textureID);
    SDL_FreeSurface(fontSurface);  // clean up
    SDL_FreeSurface(destSurface);
  end;

  procedure DisplayBMPimageXYSizeCM(pSurface: PSDL_Surface; xCM, yCM, screenWidthCM, screenHeightCM, widthCM, HeightCM: Single);
  var
    destSurface: PSDL_Surface;  // surface into which Texture image is blitted to give correct RGB order of bytes
    textureID: GLuint;
  begin
    if pSurface = Nil then Exit;
    glMatrixMode(GL_PROJECTION);
    glPushMatrix(); glLoadIdentity;
      glMatrixMode(GL_MODELVIEW);
      glPushMatrix(); glLoadIdentity;
        glOrtho(-screenWidthCm/2,screenWidthCm/2,-screenHeightCm/2,screenHeightCm/2,-1,1);
        glTranslatef(xCM, yCM, 0);
        glScaled(widthCM * 0.5, HeightCM * 0.5, 1.0);
        destSurface := SDL_CreateRGBSurface(0, pSurface^.w, pSurface^.h, 24, $000000ff,$0000ff00,$00ff0000,$ff000000);
        SDL_BlitSurface(pSurface, Nil, destSurface, Nil);
        glEnable(GL_TEXTURE_2D);                               // Create storage space for the texture
        glGenTextures(1, @textureID);                          // Create The Texture
        glBindTexture(GL_TEXTURE_2D, textureID);               // Typical Texture Generation Using Data From The Bitmap
        glTexImage2D(GL_TEXTURE_2D, 0, 3, destSurface^.w, destSurface^.h, 0, GL_RGB, GL_UNSIGNED_BYTE, destSurface^.pixels);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);  // Linear Filtering
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);        // Select how the texture image is combined with existing image
        glBegin(GL_QUADS);
          glTexCoord2f(0,0); glVertex2f(-1.0 ,1.0);        // top left
          glTexCoord2f(0,1); glVertex2f(-1.0,-1.0);        // bottom left
          glTexCoord2f(1,1); glVertex2f(1.0,-1.0);        // bottom right
          glTexCoord2f(1,0); glVertex2f(1.0, 1.0);     // top right
        glEnd;
        glDisable(GL_TEXTURE_2D);
        glDeleteTextures(1, @textureID);
        SDL_FreeSurface(destSurface);               // clean up
      glPopMatrix();
      glMatrixMode(GL_PROJECTION);
    glPopMatrix();
  end;

  procedure MakeDisplayLists(aPlaceHolderRadiusCM, aBackgroundRadiusCM, aShapeSizeCM: Single);
  var
    r, r1, r2, r3: Single;
  begin
     dlCircle := glGenLists(1);
     glNewList(dlCircle, GL_COMPILE);
       Circle(0, 0, -DistancePoint, aPlaceHolderRadiusCM, Tan(0.0629 * PiOver180) * Distance, 20, True);
     glEndList;

     dlCircleOutline := glGenLists(1);
     glNewList(dlCircleOutline, GL_COMPILE);
       Circle(0, 0, -DistancePoint, DegToCM(HalfPlaceholderDiameterDeg, Distance), DegToCM(0.05, Distance), 20, False);
     glEndList;
     // shapes are based on an area of 1.471814539 cm2
     // Shape 1, Diamond with equal area to bar
     r1 := aShapeSizeCM / 2;
     r := Sqrt(2 * Sqr(r1)); // hypotenuse for a triangle with 2 legs of length (shapeSizeCm/2)
     dlDiamond := glGenLists(1);
     glNewList(dlDiamond, GL_COMPILE);
       Bar(0, 0, -DistancePoint, r, r, PiOver4);
     glEndList;
     // Shape 2, Hexagon with equal area to bar
     dlHex := glGenLists(1);
     glNewList(dlHex, GL_COMPILE);
       Circle(0, 0, -DistancePoint, r1, r1*0.01, 6, True);
     glEndList;
     // Shape 3, Triangle, area equal to bar
     r3 := Sqrt(3)/3 * aShapeSizeCM; // Equilateral_triangle, with a side equal the shapeSizeCM
     dlTriangle := glGenLists(1);
     glNewList(dlTriangle, GL_COMPILE);
       Circle(0, 0, -DistancePoint, r3, r3*0.01, 3, True);
     glEndList;
     // Shape 4, Box, area equal to bar
     dlBox := glGenLists(1);
     glNewList(dlBox, GL_COMPILE);
       glPushMatrix;
       glMatrixMode(GL_MODELVIEW);
       glRotatef(45, 0, 0, 1);
       Circle(0, 0, -DistancePoint, r, r*0.66, 4, False);
       glPopMatrix;
     glEndList;
     // Shape 5, Ring, area equal to bar
     dlRing := glGenLists(1);
     glNewList(dlRing, GL_COMPILE);
       Circle(0, 0, -DistancePoint, r1, r1*0.5, 20, False);
     glEndList;
     // Shape 6, Plus, area equal to bar
     r1 := shapeSizeCM;
     r2 := r1 * 0.33;
     dlCross := glGenLists(1);
     glNewList(dlCross, GL_COMPILE);
       Bar(0, 0, -DistancePoint, r1, r2, 0);
       Bar(0, 0, -DistancePoint, r2, r1, 0);
     glEndList;
     // Shape 7, Horiz bar
     r2 := r1 * 0.5;
     dlBarHoriz := glGenLists(1);
     glNewList(dlBarHoriz, GL_COMPILE);
       Bar(0, 0, -DistancePoint, r1, r2, 0);
     glEndList;
     // Shape 8, Vert bar
     dlBarVert := glGenLists(1);
     glNewList(dlBarVert, GL_COMPILE);
       Bar(0, 0, -DistancePoint, r2 , r1, 0);
     glEndList;
     // Shape 9, Square with area equal to bar
     dlSquareEa:= glGenLists(1);
     glNewList(dlSquareEa, GL_COMPILE);
       bar(0, 0, -DistancePoint, r1, r1, 0);
     glEndList;
     // Shape 10, circle with area equal to bar
     dlCircleEa := glGenLists(1);
     glNewList(dlCircleEa, GL_COMPILE);
       Circle(0, 0, -DistancePoint, r2, 0, 20, True);
     glEndList;
     // Shape 11, // Star with radius equal to placeholder's radius. Area of 0.82627615cm2 on AOC monitor at 71cm
     r := PentagonSideToRadius * r2 / Sin(DegToRad(HalfPentagonAngle));
     dlStar := glGenLists(1);
     glNewList(dlStar, GL_COMPILE);
       Pentagram(0, 0, -DistancePoint, r);
     glEndList;

     dlCircleBackground := glGenLists(1);
     glNewList(dlCircleBackground, GL_COMPILE);
       Circle(0, 0, -Distance-0.2, aBackgroundRadiusCM, 0, 100, True);
     glEndList;
   end;

  procedure ShowCountdown;
  begin
    glPushMatrix();
    glViewport(0, 0, ud.monWidth, ud.monHeight);
    glMatrixMode(GL_PROJECTION); glLoadIdentity;
    glMatrixMode(GL_MODELVIEW);  glLoadIdentity;
    glColor3f(1, 1, 1);
    glOrtho(-ud.HalfWidth, ud.HalfWidth, -ud.HalfHeight, ud.HalfHeight, -1, 1);
 //   RenderText(pfGeneral, 'Trials remaining: ' + IntToStr(Ntrials - trialIdx), -ud.monWidth*0.25, -ud.monHeight*0.5, SDLColorWhite, 1);
    glPopMatrix();
  end;

  procedure DrawTextStim(pfont: PTTF_Font; fontCol: TColourReal; x, y: Single; const {%H-}aText: String);
  begin
    x := x * ud.WidthToWCMRatio;      // convert screen cm locations to pixel locations
    y := y * ud.HeightToHCMRatio;
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity;
    glViewport(0, 0, ud.monWidth, ud.monHeight);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity;
    glOrtho(-ud.HalfWidth, ud.HalfWidth, -ud.HalfHeight, ud.HalfHeight, -1, 1);
    RenderTextWithBackColorCentred(pfont, aText, fontCol.ToSDLColor(255), backgroundfColor, x, y, 1);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
  end;

  procedure DrawFixationWithPlaceholders(aPlaceholdersColour: TColourReal; drawFixSpot: Boolean=True);
  var
    x, y: Single;
  begin
    glMatrixMode(GL_MODELVIEW); glLoadIdentity;
    if drawFixSpot then
      DrawFixationSpot(fixSpotRadiusCM, FixationColour);
    glColor3f(aPlaceholdersColour.r, aPlaceholdersColour.g, aPlaceholdersColour.b);
    glMatrixMode(GL_MODELVIEW); glLoadIdentity;
    ObjectLocation(x, y, targetRadiusCM, 1);
    glTranslatef(x, y, 0);
    glCallList(dlCircleOutline);

    glMatrixMode(GL_MODELVIEW); glLoadIdentity;
    ObjectLocation(x, y, targetRadiusCM, 2);
    glTranslatef(x, y, 0);
    glCallList(dlCircleOutline);

    glMatrixMode(GL_MODELVIEW); glLoadIdentity;
    ObjectLocation(x, y, targetRadiusCM, 3);
    glTranslatef(x, y, 0);
    glCallList(dlCircleOutline);

    glMatrixMode(GL_MODELVIEW); glLoadIdentity;
    ObjectLocation(x, y, targetRadiusCM, 4);
    gltranslatef(x, y, 0);
    glCallList(dlCircleOutline);
  end;

  procedure DrawBackgroundFixationWithPlaceholders(fixSpotSizeCM, targetRadiusCM: Single;
                             contextColour, aFixationColour, aPlaceholdersColour: TColourReal);
  var
    x, y: Single;
  begin
    glMatrixMode(GL_MODELVIEW); glLoadIdentity;
    DrawFixationSpot(fixSpotSizeCM, aFixationColour);
    glColor3f(contextColour.r, contextColour.g, contextColour.b);
    glCallList(dlCircleBackground);
    glColor3f(aPlaceholdersColour.r, aPlaceholdersColour.g, aPlaceholdersColour.b);
    glMatrixMode(GL_MODELVIEW); glLoadIdentity;
    ObjectLocation(x, y, targetRadiusCM, 1);
    glTranslatef(x,y,0); glCallList(dlCircleOutline);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity;
    objectLocation(x,y,targetRadiusCM,2);
    gltranslatef(x,y,0);
    glCallList(dlCircleOutline);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity;
    objectLocation(x,y,targetRadiusCM,3);
    gltranslatef(x,y,0);
    glCallList(dlCircleOutline);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity;
    objectLocation(x,y,targetRadiusCM,4);
    gltranslatef(x,y,0);
    glCallList(dlCircleOutline);
  end;

  procedure DrawShape(shapeNo: Integer);
  begin
    case shapeNo of
      1:  glCallList(dlDiamond);
      2:  glCallList(dlHex);
      3:  glCallList(dlTriangle);
      4:  glCallList(dlBox);
      5:  glCallList(dlRing);
      6:  glCallList(dlCross);
      7:  glCallList(dlBarHoriz);
      8:  glCallList(dlBarVert);
      9:  glCallList(dlSquareEa);
      10: glCallList(dlCircleEa);
      11: glCallList(dlStar);
    end;
  end;

  procedure DrawShapeOrChar(shapeNo: Integer; shapeCol: TColourReal; x, y, imageSizeCm: Single);
  begin
    case shapeNo of
      0..32: begin
               glColor3f(shapeCol.r, shapeCol.g, shapeCol.b);
               DrawShape(shapeNo);
             end;
      33..255:  DrawTextStim(pfStimulus, shapeCol, x, y, Char(shapeNo));
      300..397: DisplayBMPimageXYSizeCM(surfaceArr[shapeNo-300], x, y, ud.monWidthCM, ud.monHeightCM, imageSizeCm, imageSizeCm);
    end;
  end;

  procedure DrawS2Cues;
  var
    c, shape, col: Integer;
    x, y: Single;
  begin
    for c := 1 to 5 do
      begin
        case c of
          1: begin shape := ti[s2ShapePos1];  col := ti[s2ColPos1]; end;
          2: begin shape := ti[s2ShapePos2];  col := ti[s2ColPos2]; end;
          3: begin shape := ti[s2ShapePos3];  col := ti[s2ColPos3]; end;
          4: begin shape := ti[s2ShapePos4];  col := ti[s2ColPos4]; end;
          5: begin shape := ti[s2ShapePos5];  col := ti[s2ColPos5]; end;
        end;
        glMatrixMode(GL_MODELVIEW); glLoadIdentity;
        ObjectLocation(x, y, s2SampleRadiusCM, c);
        glTranslatef(x, y, 0);
        DrawShapeOrChar(shape{%H-}, Colours[{%H-}col], x, y, imageSizeCM);
      end;
  end;

  procedure ShowTextFeedback(pfont: PTTF_Font; fontCol, bgrCol: TSDL_Color; const {%H-}text: String);
  begin
    glPushMatrix();
    glViewport(0, 0, ud.monWidth, ud.monHeight);
    glMatrixMode(GL_PROJECTION); glLoadIdentity;
    glMatrixMode(GL_MODELVIEW);  glLoadIdentity;
    glOrtho(-ud.HalfWidth, ud.HalfWidth, -ud.HalfHeight, ud.HalfHeight, -1, 1);
    RenderTextWithBackColorCentred(pfont, text, fontCol, bgrCol, 0, 0, 1);
    glPopMatrix();
  end;

  procedure HandleSuspended;
  var
    c: Integer;
  begin
    while isSuspended do
      begin
        PollEvent;
        ef.ProjectionTrans;
        glMatrixMode(GL_MODELVIEW); glLoadIdentity;
        DrawBackgroundFixation(PauseBackgroundCircleColour);
        DrawFixationWithPlaceholders(ud.HoldersColor);
        glPushMatrix();
        glViewport(0, 0, ud.monWidth, ud.monHeight);
        glMatrixMode(GL_PROJECTION); glLoadIdentity;
        glMatrixMode(GL_MODELVIEW);  glLoadIdentity;
        glColor3f(1, 1, 1);
        glOrtho(-ud.HalfWidth, ud.HalfWidth, -ud.HalfHeight, ud.HalfHeight, -1, 1);
        RenderTextWithBackColorCentred(pfGeneral, 'PAUSED', SDLColorWhite, RealColorBlack.ToSDLColor(0), 0, 0, 1);
        glPopMatrix();
        ef.Render;
        rr.isRuinedTrial := 1; // flag that the trial is garbage

        if not isSuspended then
          for c := 1 to Round(2 / ud.ReciprocalRefresh) do  // 2 sec pause   after resuming from suspense
            begin
              ef.ProjectionTrans;
              glMatrixMode(GL_MODELVIEW); glLoadIdentity;
              DrawBackgroundFixation(RunBackgroundCircleColour);
              DrawFixationWithPlaceholders(ud.HoldersColor);
              ef.Render;
            end;
      end;
  end;

  procedure PossiblyGetTMSOnsetTime;
  begin
    if rr.TMSOnsetTime = -1 then
      begin
        if cumFrameCount = TMSFrameIdx then
          rr.TMSOnsetTime := SDL_GetTicks - timeOfExperimentStart;
        Inc(cumFrameCount);
      end;
  end;

  procedure HandlePauseTrial;
  var
    c: Integer;

     function GetPauseTrialMessage: String;
     begin
       case ud.reportTrialPogress of
         False: case trialIdx of
                  0:   Exit('Training block of trials');
                  16:  Exit('Ready for the actual experiment?');
                  48:  Exit('Pause: 87.5% of trials left');
                  80:  Exit('Pause: 75% of trials left');
                  112: Exit('Pause: 62.5% of trials left');
                  144: Exit('Pause: 50% of trials left');
                  176: Exit('Pause: 37.5% of trials left');
                  208: Exit('Pause: 25% of trials left');
                  240: Exit('Pause: 12.5% of trials left');
                  else Exit('error in pause trial determination!');
                end;
         True:  case trialIdx of
                  0:   Exit('Training block of trials');
                  16:  Exit('Ready for the actual experiment?');
                  48:  Exit('Pause: 87.5% of trials left (224 trials)');
                  80:  Exit('Pause: 75% of trials left (192 trials)');
                  112: Exit('Pause: 62.5% of trials left (160 trials)');
                  144: Exit('Pause: 50% of trials left (128 trials)');
                  176: Exit('Pause: 37.5% of trials left (96 trials)');
                  208: Exit('Pause: 25% of trials left (64 trials)');
                  240: Exit('Pause: 12.5% of trials left (32 trials)');
                  else Exit('error in pause trial determination!');
                end;
       end;
     end;

     procedure ShowMessageAndPoll;
     begin
       state := -1;
       CheckKeysAndButtons;
       ef.ProjectionTrans;
       DrawBackgroundFixation(PauseBackgroundCircleColour);
       DrawFixationWithPlaceholders(ud.HoldersColor);
       glPushMatrix();
         glViewport(0, 0, ud.monWidth, ud.monHeight);
         glMatrixMode(GL_PROJECTION); glLoadIdentity;
         glMatrixMode(GL_MODELVIEW);  glLoadIdentity;
         glColor3f(1, 1, 1);
         glOrtho(-ud.HalfWidth, ud.HalfWidth, -ud.HalfHeight, ud.HalfHeight, -1, 1);
         RenderTextWithBackColorCentred(pfGeneral, GetPauseTrialMessage, SDLColorWhite, RealColorBlack.ToSDLColor(0), 0, 0, 1);
       glPopMatrix();
       ef.Render;
     end;

  begin
    if (trialIdx < 256) and (Byte(trialIdx) in PauseTrialIndexes) then
      begin
        case trialIdx of
          0: begin
               repeat
                 ShowMessageAndPoll;
               until (Byte(state) in [SDL_BUTTON_LEFT, SDL_BUTTON_RIGHT]);
               if isVeryFirstTrial then
                 begin
                   timeOfExperimentStart := SDL_GetTicks;
                   isVeryFirstTrial := False;
                 end;
             end;
          else
            repeat
              ShowMessageAndPoll;
            until state <> -1;
        end;
        for c:= 1 to Round(2 / ud.ReciprocalRefresh) do// 2 sec pause
        begin
          ef.ProjectionTrans;
          DrawBackgroundFixation(RunBackgroundCircleColour);
          DrawFixationWithPlaceholders(ud.HoldersColor);
          HandleSuspended;
          PollEvent;
          ef.Render;
        end;
      end;
  end;

  procedure DoS1;
  var
    fIdx: Integer;
  begin
    cumFrameCount := 0;
    rr.s1OnsetTime := SDL_GetTicks - timeOfExperimentStart;   // get the time the first image is displayed
    for fIdx := 1 to Round(ti[s1Dur] /1000 / ud.ReciprocalRefresh) do
      begin
        ef.ProjectionTrans;
        DrawBackgroundFixation(RunBackgroundCircleColour);
        case ti[s1Quad] of
          5: begin
               glMatrixMode(GL_MODELVIEW); glLoadIdentity;
               ObjectLocation(x, y, cueRadiusCM, 1); glTranslatef(x, y, 0);
               DrawShapeOrChar(ti[s1Shape], Colours[ti[s1Col]], x, y, imageSizeCM);
               glMatrixMode(GL_MODELVIEW); glLoadIdentity;
               ObjectLocation(x, y, cueRadiusCM, 2); glTranslatef(x, y, 0);
               DrawShapeOrChar(ti[s1Shape], Colours[ti[s1Col]], x, y, imageSizeCM);
               glMatrixMode(GL_MODELVIEW); glLoadIdentity;
               ObjectLocation(x, y, cueRadiusCM, 3); glTranslatef(x, y, 0);
               DrawShapeOrChar(ti[s1Shape], Colours[ti[s1Col]], x, y, imageSizeCM);
               glMatrixMode(GL_MODELVIEW); glLoadIdentity;
               ObjectLocation(x, y, cueRadiusCM, 4); glTranslatef(x, y, 0);
               DrawShapeOrChar(ti[s1Shape], Colours[ti[s1Col]], x, y, imageSizeCM);
             end;
          6: begin  //cue in screen centre
               glMatrixMode(GL_MODELVIEW); glLoadIdentity;
               ObjectLocation(x, y, 0, 5); glTranslatef(0, 0, 0);
               DrawShapeOrChar(ti[s1Shape], Colours[ti[s1Col]], x, y, imageSizeCM);
             end;
          else
            glMatrixMode(GL_MODELVIEW); glLoadIdentity;
            objectLocation(x, y, cueRadiusCM, ti[s1Quad]); glTranslatef(x, y, 0);
            DrawShapeOrChar(ti[s1Shape], Colours[ti[s1Col]], x, y, imageSizeCM);
        end;
        glClear(GL_DEPTH_BUFFER_BIT);
        DrawFixationWithPlaceholders(ud.HoldersColor);
        PollEvent;
        HandleSuspended; // suspend rendering the stimulus if IS_SUSPENDED
        ef.Render;
        PossiblyGetTMSOnsetTime;
      end;
  end;

  procedure DoS1S2ISI;
  var
    frames: Integer;
  begin
    for frames := 1 to Round(ti[s1s2ISI]/1000 / ud.ReciprocalRefresh) do
      begin
        ef.ProjectionTrans;
        DrawBackgroundFixationWithPlaceholders(fixSpotRadiusCM, targetRadiusCM, RunBackgroundCircleColour, FixationColour, ud.HoldersColor);
        PollEvent;
        HandleSuspended; // suspend rendering the stimulus if IS_SUSPENDED
        ef.Render;
        PossiblyGetTMSOnsetTime;
      end;
  end;

  procedure DoS2;
  var
    frame: Integer;
  begin
    rr.s2OnsetTime := SDL_GetTicks - timeOfExperimentStart;
    for frame := 1 to Round(ti[s2Dur]/1000/ud.ReciprocalRefresh) do
      begin
        ef.ProjectionTrans;
        glMatrixMode(GL_MODELVIEW); glLoadIdentity;
        DrawBackgroundFixation(RunBackgroundCircleColour);
        DrawS2Cues;
        glClear(GL_DEPTH_BUFFER_BIT);
        DrawFixationWithPlaceholders(ud.HoldersColor);
        PollEvent;
        HandleSuspended;
        ef.Render;
        PossiblyGetTMSOnsetTime;
      end;
  end;

  procedure DoS2S3ISIBlank;
  var
    frame: Integer;
  begin
    for frame := 1 to Round(ti[s2s3ISI]/1000/ud.ReciprocalRefresh) do
      begin
        ef.ProjectionTrans;
        DrawBackgroundFixationWithPlaceholders(fixSpotRadiusCM, targetRadiusCM, RunBackgroundCircleColour, FixationColour, ud.HoldersColor);
        PollEvent;
        HandleSuspended;
        ef.Render;
        PossiblyGetTMSOnsetTime;
     end;
  end;

  procedure TargetImage(targetRadiusCM: Single; targetShape, targetQuadrant, distractorShape,
                        target_colour, distractor_colour: Integer;
                        drawPeripheral, showPlaceholderCentre: Boolean;
                        imageSizeCm, periperRadiusCM: Single);
  var
    x, y, sinTheta, cosTheta: Single;
    c: Integer;
  begin
    if periperRadiusCM < 0 then
      periperRadiusCM := targetRadiusCM;
    for c := 1 to 16 do
      begin
        if (targetShape = 0) or not( ((c=2) and (targetQuadrant=2)) or ((c=6) and (targetQuadrant=3)) or ((c=10) and (targetQuadrant=4)) or  ((c=14) and (targetQuadrant=1)) ) then
          begin
            if drawPeripheral then      // draw distractors
              begin
                SinCos(PiOver8 * c, sinTheta, cosTheta);
                x :=  periperRadiusCM * sinTheta;
                y :=  periperRadiusCM * cosTheta;
                glMatrixMode(GL_MODELVIEW); glLoadIdentity;
                glTranslatef(x, y, 0);
                // draw distractor shape
                DrawShapeOrChar(distractorShape, Colours[distractor_colour], x, y, imageSizeCm);
                glColor3f(ud.HoldersColor.r, ud.HoldersColor.g, ud.HoldersColor.b);  // draw outline of circle
                glCallList(dlCircleOutline);
              end;
          end;

        if (targetshape <> 0) then      // draw target
          begin
            glMatrixMode(GL_MODELVIEW); glLoadIdentity;
            ObjectLocation(x, y, targetRadiusCM, targetQuadrant);
            glTranslatef(x, y, 0);
            DrawShapeOrChar(targetShape, Colours[target_colour], x, y, imageSizeCm);
            if (showPlaceholderCentre or (targetQuadrant <> 5)) then  // draw outline of circle
              begin
                glColor3f(ud.HoldersColor.r, ud.HoldersColor.g, ud.HoldersColor.b);
                glCallList(dlCircleOutline);
              end;
          end;
    end;
  end;

  procedure DoS3;       //     S3 spatial cue onset
  var
    frame: Integer;
  begin
    rr.s3OnsetTime := SDL_GetTicks - timeOfExperimentStart;
    for frame := 1 to Round(ti[s3Dur]/1000/ud.ReciprocalRefresh) do
    begin
      ef.ProjectionTrans;
      DrawBackgroundFixation(RunBackgroundCircleColour);
      case ti[s3Shape] of
        12: ;
        else TargetImage(targetRadiusCM, ti[s3Shape], ti[s3quad], ti[s3DistShape], ti[s3Col],
                         ti[s3DistCol], ud.ShowS3Placeholders, False, imageSizeCM, -1);
      end;
      glClear(GL_DEPTH_BUFFER_BIT);
      DrawFixationWithPlaceholders(ud.HoldersColor, ti[s3Quad] <> 5);
      PollEvent;
      HandleSuspended;
      ef.Render;
      PossiblyGetTMSOnsetTime;
    end;
  end;

  procedure DoS3S4ISI;  //     S3 - S4 ISI Blank screen
  var
    frame: Integer;
  begin
    for frame := 1 to Round(ti[s3s4ISI]/1000/ud.ReciprocalRefresh) do
      begin
        ef.ProjectionTrans;
        DrawBackgroundFixationWithPlaceholders(fixSpotRadiusCM, targetRadiusCM, RunBackgroundCircleColour, FixationColour, ud.HoldersColor);
        PollEvent;
        HandleSuspended;
        ef.Render;
        PossiblyGetTMSOnsetTime;
      end;
  end;

  procedure DoS4;       // S4  target onset
  var
    frame: Integer;
  begin
    rr.t1 := SDL_GetTicks;
    rr.s4OnsetTime := rr.t1 - timeOfExperimentStart;
    hasResponded := False;
    for frame := 1 to Round(ti[s4Dur]/1000/ud.ReciprocalRefresh) do
    begin
      ef.ProjectionTrans;
      case ud.exptNo of
        1: DrawBackgroundFixation(RunBackgroundCircleColour);
        2: DrawBackgroundFixationWithPlaceholders(fixSpotRadiusCM, targetRadiusCM, RunBackgroundCircleColour, FixationColour, ud.HoldersColor);
      end;
      case ti[s4Shape] of
        12: ;
        else  TargetImage(targetRadiusCM, ti[s4Shape], ti[s4Quad], ti[s4DistShape], ti[s4Col], ti[s4DistCol],
                          False, False, imageSizeCM, -1);
      end;
      glClear(GL_DEPTH_BUFFER_BIT);
      DrawFixationWithPlaceholders(ud.HoldersColor, ti[s4Quad] <> 5);
      HandleSuspended;
      ef.Render;
      CheckKeysAndButtons;
      if not hasResponded and (state <> -1) then
        begin
          hasResponded := True;
          rr.rState := state;
          rr.rEventTime := eventTime;
        end;
      PossiblyGetTMSOnsetTime;
    end;
  end;

  procedure AppendTrialDataToFile;
  var
    tf: TextFile;
    fac: ETrialStr;
  begin
    AssignFile(tf, ud.OutputFilename);
    Append(tf);
    Write(tf, ud.ExperimentName,#9,ud.DateStr,#9,ud.TimeStr,#9,IntToStr(ud.inputFileNo),#9,ud.participantID,#9,
          ud.age,#9,ud.sex,#9,ud.handed,#9,ud.monitorName,#9,'0'#9,ud.sessionNo.ToString,#9,
          IntToStr(ti[s1Marker]),#9,IntToStr(ti[s2Marker]),#9,IntToStr(ti[s3Marker]),#9,IntToStr(ti[s4Marker]),#9,
          IntToStr(ti[s1Shape]),#9,IntToStr(ti[s1Quad]),#9,FloatToStr(ti[s1Dur]),#9,FloatToStr(ti[s1s2ISI]),#9,
          IntToStr(ti[s2ShapePos1]),#9,IntToStr(ti[s2ShapePos2]),#9,IntToStr(ti[s2ShapePos3]),#9,
          IntToStr(ti[s2ShapePos4]),#9,IntToStr(ti[s2ShapePos5]),#9,FloatToStr(ti[s2Dur]),#9,FloatToStr(ti[s2s3ISI]),#9,
          IntToStr(ti[s3Shape]),#9,IntToStr(ti[s3DistShape]),#9,IntToStr(ti[s3Quad]),#9,FloatToStr(ti[s3Dur]),#9,
          FloatToStr(ti[s3s4ISI]),#9,IntToStr(ti[s4Shape]),#9,IntToStr(ti[s4DistShape]),#9,IntToStr(ti[s4Quad]),#9,
          FloatToStr(ti[s4Dur]),#9,FloatToStr(ti[ResponseTimeAfterS4]),#9,IntToStr(ti[FeedbackShape]),#9,
          FloatToStr(ti[FeedbackDurAfterResponseTime]),#9,FloatToStr(ti[ITIAfterFeedback]),#9,IntToStr(ti[s1Col]),#9,
          IntToStr(ti[s2ColPos1]),#9,IntToStr(ti[s2ColPos2]),#9,IntToStr(ti[s2ColPos3]),#9,IntToStr(ti[s2ColPos4]),#9,
          IntToStr(ti[s2ColPos5]),#9,IntToStr(ti[s3Col]),#9,IntToStr(ti[s3DistCol]),#9,IntToStr(ti[s4Col]),#9,
          IntToStr(ti[s4DistCol]),#9,IntToStr(ti[keyMap]),#9,IntToStr(ti[task]),#9,IntToStr(ti[TMSs3SOA]),#9,
          ts[expCondition],#9);
    Write(tf, IntToStr(rr.Response),#9,IntToStr(rr.CorrectResponse),#9,IntToStr(rr.RTms),#9,IntToStr(rr.RTmsLessConstError),#9,
          IntToStr(rr.s1OnsetTime),#9,IntToStr(rr.s2OnsetTime),#9,IntToStr(rr.s3OnsetTime),#9,IntToStr(rr.TMSOnsetTime),#9,
          IntToStr(rr.s4OnsetTime),#9,IntToStr(rr.responseOnsetTime),#9,IntToStr(rr.feedbackOnsetTime),#9,
          IntToStr(rr.blankOnsetTime),#9,IntToStr(rr.isRuinedTrial),#9);
    for fac := factor1 to factor9 do
      Write(tf, ts[fac],#9);
    WriteLn(tf);
    CloseFile(tf);
  end;

  procedure ProcessResponse;  //'blank' screen duration
  var
    //isAuditoryFeedback: Boolean = True; // n/a
    isVisualFeedback: Boolean = False;
    doResponseCheck: Boolean = True;
    StopResponseChecking: Boolean = False;
    frame: Integer;
    frameTime: Single;
  begin
    for frame := 1 to nResponseFrames do
      begin
        PossiblyGetTMSOnsetTime;
        frameTime := frame * ud.ReciprocalRefresh;
        StopResponseChecking := frameTime >= rtAfterS4Over1000;
        if StopResponseChecking and (rr.feedbackOnsetTime = -1) then
          rr.feedbackOnsetTime := SDL_GetTicks - timeOfExperimentStart;
        if (frameTime > rtMax) and (rr.blankOnsetTime = -1) then
          rr.blankOnsetTime := SDL_GetTicks - timeOfExperimentStart;

        ef.ProjectionTrans;
        DrawBackgroundFixationWithPlaceholders(fixSpotRadiusCM, targetRadiusCM, RunBackgroundCircleColour, FixationColour, ud.HoldersColor);
        HandleSuspended;
        if doResponseCheck then
          case hasResponded of
            False: begin
                     CheckKeysAndButtons;
                     if (state <> -1) then
                       begin
                         hasResponded := True;
                         rr.rState := state;
                         rr.rEventTime := eventTime;
                       end;
                   end;
            True:  begin    //if there has been a response
                     if doResponseCheck then
                       doResponseCheck := False;
                     rr.RTms := rr.rEventTime - rr.t1; // record Reaction Time  between response event and time t1 taken immediately after s4 render command is sent;
                     rr.responseOnsetTime := rr.rEventTime - timeOfExperimentStart;  // record response time relative to experiment start
                     case ti[keyMap] of
                       0: if rr.rState = ResponseLeftButton then
                            rr.Response := 0  // 'different'
                          else if rr.rState = ResponseRightButton then
                            rr.Response := 1; // 'same'
                       1: if rr.rState = ResponseLeftButton then
                            rr.Response := 1  // 'same'
                          else if rr.rState = ResponseRightButton then
                            rr.Response := 0; // 'different'
                     end;
                     case ti[task] of  // check correctness of response
                       1, 2: case targetMatchesCue of          // "same or different" task
                               True: if rr.Response = 1 then
                                       rr.CorrectResponse := 1
                                     else if rr.Response = 0 then
                                       rr.CorrectResponse := 0;
                               False: if rr.Response = 1 then
                                        rr.CorrectResponse := 0
                                      else if rr.Response = 0 then
                                        rr.CorrectResponse := 1;
                             end;
                       3: rr.CorrectResponse := rr.Response;    // Task Type 3 identification task
                     end;
                   end; // state check
          end; // doResponseCheck;
          // At Response_Time_after_S4 sec, stop checking for responses and give auditory feedback
        if StopResponseChecking then
          begin
            if doResponseCheck then
              doResponseCheck := False;     // stop checking for responses
            case ti[FeedbackShape] of
              0: ;{if isAuditoryFeedback then    // auditory feedback - does not apply to Exp1 or Exp 2 where FeedbackShape is always 13
                   begin
                     case rr.CorrectResponse of
                       1: Mix_PlayChannel(Ord(CorrectWav),   sounds[CorrectWav], 0);
                       0: Mix_PlayChannel(Ord(IncorrectWav), sounds[IncorrectWav], 0);
                     end;
                     isAuditoryFeedback := False;
                   end; }
              else                            // visual feedback
                isVisualFeedback := frameTime <= rtMax;
                if isVisualFeedback then
                  case rr.CorrectResponse of
                    1: case ti[FeedbackShape] of
                         1..11: begin
                                  glMatrixMode(GL_MODELVIEW); glLoadIdentity;
                                  glTranslatef(x, y, 0);
                                  glColor3f(CorrectFeedbackColour.r, CorrectFeedbackColour.g, CorrectFeedbackColour.b);
                                  DrawShape(ti[FeedbackShape]);
                                end;
                         12: ShowTextFeedback(pfFeedback, correctfColor, backgroundfColor, rsCorrect);
                         13: DisplayBMPimageXYSizeCM(surfaceArr[CorrectBMP], x, y, ud.monWidthCM, ud.monHeightCM, imageFeedbackCM, imageFeedbackCM);
                       end;
                    0: case ti[FeedbackShape] of
                         1..11: begin
                                  glMatrixMode(GL_MODELVIEW); glLoadIdentity;
                                  glTranslatef(x, y, 0);
                                  glColor3f(IncorrectFeedbackColour.r, IncorrectFeedbackColour.g, IncorrectFeedbackColour.b);
                                  DrawShape(ti[FeedbackShape]);
                                end;
                         12: ShowTextFeedback(pfFeedback, incorrectfColor, backgroundfColor, rsIncorrect);
                         13: DisplayBMPimageXYSizeCM(surfaceArr[IncorrectBMP], x, y, ud.monWidthCM, ud.monHeightCM, imageFeedbackCM, imageFeedbackCM);
                      end;
                  end;
            end; // case FeedbackShape;
          end;   // StopResponseChecking
        ef.Render;
      end;  // this trial is complete
      case rr.Response of
        -1:  rr.RTmsLessConstError := -1;
        else rr.RTmsLessConstError := rr.RTms - RTconstantErrorMS;
      end;
      AppendTrialDataToFile;
  end;

  procedure CheckForTrainingCompleted;
  begin
    case trialIdx of
      15: begin      // find the number of correct responses in the training trials
            case rr.Accuracy < MinimumTrainingAccuracy of
              True:  begin
                       trialIdx := 0;
                       rr.CumCorrectResp := 0; // reset Accuracy to 0
                     end;
              False: Inc(trialIdx);
            end;
            SDL_HideWindow(ef.SDLWindow);
            MainForm.wiz.SetAccuracy(rr.Accuracy);
          end;
      else
        Inc(trialIdx);
      end;
  end;

begin
  InitialiseParameters;
  c := SetupFonts(pfGeneral, pfStimulus, pfFeedback);
  if c < 0 then
    Exit(c);
  if not LoadSurfaceArrayOK(surfaceArr) then
    Exit(-5);
  c := ef.SetupSDL;
  if c < 0 then
    Exit(c);
  MakeDisplayLists(placeholderRadiusCM, backgroundRadiusCM, shapeSizeCM);

  trialIdx := 0;
  isSuspended := False;
  isVeryFirstTrial := True;
  try
    repeat
      hasResponded := False;
      rr.ReInit;
      ti := trialArr[trialIdx].i;
      ts := trialArr[trialIdx].s;
      TMSFrameIdx := Round((ti[s1Dur] + ti[s1s2ISI] + ti[s2Dur] + ti[s2s3ISI] + ti[TMSs3SOA])/1000/ud.ReciprocalRefresh);
      nResponseFrames := Round((ti[ResponseTimeAfterS4] + ti[FeedbackDurAfterResponseTime] + ti[ITIAfterFeedback])/1000/ud.ReciprocalRefresh);
      rtAfterS4Over1000 := ti[ResponseTimeAfterS4]/1000;
      rtMax := rtAfterS4Over1000 + ti[FeedbackDurAfterResponseTime]/1000;
      case ti[task] of
        1: targetMatchesCue :=                       // shape matching task
              (ti[s2ShapePos1] = ti[s4Shape]) or
              (ti[s2ShapePos2] = ti[s4Shape]) or
              (ti[s2ShapePos3] = ti[s4Shape]) or
              (ti[s2ShapePos4] = ti[s4Shape]) or
              (ti[s2ShapePos5] = ti[s4Shape]);
        2: targetMatchesCue :=                       // colour matching task
              ((ti[s2ShapePos1] <> 0) and (ti[s2ColPos1] = ti[s4Col])) or
              ((ti[s2ShapePos2] <> 0) and (ti[s2ColPos2] = ti[s4Col])) or
              ((ti[s2ShapePos3] <> 0) and (ti[s2ColPos3] = ti[s4Col])) or
              ((ti[s2ShapePos4] <> 0) and (ti[s2ColPos4] = ti[s4Col])) or
              ((ti[s2ShapePos5] <> 0) and (ti[s2ColPos5] = ti[s4Col]));
      end;
      HandlePauseTrial;
      DoS1;
      DoS1S2ISI;
      DoS2;
      DoS2S3ISIBlank;
      DoS3;
      DoS3S4ISI;
      DoS4;
      ProcessResponse;
      CheckForTrainingCompleted;
      //rr.ConsoleReport;
    until trialIdx > MaxTrialIdx;

    experimentResult := 0;
    ef.UnsetSDL;
    MainForm.BringToFront;
    MainForm.wiz.ShowFinalPage;
  except
    on e: ExperimentTerminateException do
      begin
        ef.UnsetSDL;
        experimentResult := -1;
        MainForm.BringToFront;
        MainForm.wiz.ShowFinalPage;
      end;
  end;
end;

end.

